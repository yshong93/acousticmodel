# from KoG2P.YSg2p import runKoG2P

import os
import re

class Script(object):

    def __init__(self, dir = 'Data', file_name = 'script_nmbd_by_sentence.txt'):
        self.script_path = os.path.join(dir, file_name)
        # print(self.script_path)
        # self.phone_encoding = self.get_phone_encoding()
        self.scripts = self.read_script_sentence(self.script_path)

    def read_script_sentence(self, file_path):
        return_dict = {}
        with open(file_path, encoding='utf-16') as file:
            lines = file.readlines()

            regex = re.compile(r'<\d+.*>')

            story_idx = -1
            for idx, val in enumerate(lines):
                if (not val.isspace()):
                    sentence = val.strip()

                    if (regex.findall(sentence)):
                        re_test = re.compile('\d+[.]')
                        story_idx = re_test.search(sentence).group()
                        story_idx = int(story_idx[:-1])
                        #                     story_idx = story_idx + 1

                        story_dict = {}
                        return_dict[story_idx] = story_dict


                    else:
                        hangul = re.compile('[^ ㄱ-ㅣ가-힣|0-9]+')  # 한글과 띄어쓰기를 제외한 모든 글자

                        sentence_split = sentence.split('.')

                        content = '.'.join(sentence_split[1:]).strip()
                        return_dict[story_idx][int(sentence_split[0])] = hangul.sub('', content)

                        # pp.pprint(return_dict)
        return return_dict


    def get_script_from_filename(self, file_name):
        if('n' in file_name):
            # file_name = 'fx08/nfy_s06_t07_s12.wav'
            info = file_name.split('.')[0].split('/')[1]
            split_info = info.split('_')

            doc_num = split_info[2][1:]
            #     print(doc_num)

            sent_num = split_info[3][1:]

            return self.scripts[int(doc_num)][int(sent_num)]
        else:
            #     file_name = 'fv17/fv17_t03_s18.wav'
            info = file_name.split('.')[0].split('/')[1]
            split_info = info.split('_')

            doc_num = split_info[1][1:]
            sent_num = split_info[2][1:]

            return self.scripts[int(doc_num)][int(sent_num)]

    def get_script_from_num(self, doc_num, sentence_num):
        return self.scripts[doc_num][sentence_num]

    def get_phoneme_list(self, doc_num, sentence_num):
        sentence = self.scripts[doc_num][sentence_num]
        # phone_list = self.sentence_to_phones(sentence)

        return sentence


if __name__ == '__main__' :
    # print(Script(dir='../Data').get_script_from_num(1,4))
    print(Script().get_script_from_num(1, 4))
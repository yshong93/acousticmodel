import librosa
import textgrid

import random

class TextGridManager(object):
    def __init__(self):
        self.vowel_list = ['IY', 'EH', 'EY', 'AA', 'WW', 'UW', 'OW', 'AX', 'JH', 'JE', 'JA', 'JO', 'JU', 'JX', 'UI',
                           'WH', 'WE', 'OI', 'WA', 'WX', 'WI']

    def get_random_phoneme(self, path):
        tg = textgrid.TextGrid()
        tg.read(path)
        phones1 = tg.tiers[1]
        #         print(phone[0])
        phone = random.choice(phones1)
        #         print(phone.maxTime)
        min_time = int(librosa.core.time_to_samples(phone.minTime, sr=16000))
        max_time = int(librosa.core.time_to_samples(phone.maxTime, sr=16000))
        return min_time, max_time

    def get_random_phoneme_vowel(self, path):
        tg = textgrid.TextGrid()
        tg.read(path)

        phones1 = tg.tiers[1]
        while True:
            phone = random.choice(phones1)
            if phone.mark in self.vowel_list:
                min_time = int(librosa.core.time_to_samples(phone.minTime, sr=16000))
                max_time = int(librosa.core.time_to_samples(phone.maxTime, sr=16000))
                return min_time, max_time

                # def textgrid_to_ctc(self, path):
                #     tg = textgrid.TextGrid()
                #     tg.read(path)
                #
                #     phones1 = tg.tiers[1]
                #     for phone in phones1:
                #

    def get_like_list(self, path):
        interval_samples = librosa.core.samples_to_frames(samples, hop_length=512, n_fft=None)
        tg = textgrid.TextGrid()
        tg.read(path)

        phones1 = tg.tiers[1]
        while True:
            phone = random.choice(phones1)
            if phone.mark in self.vowel_list:
                min_time = int(librosa.core.time_to_samples(phone.minTime, sr=16000))
                max_time = int(librosa.core.time_to_samples(phone.maxTime, sr=16000))
                return min_time, max_time

if __name__ == '__main__':
    test_label_path = '/media/yoonseok/Data/Corpus/Audio/korean_reading/label/fv01_t01_s02.textgrid'


def ler(y_true, y_pred):
    return levenshteinDistance(y_pred, y_true) / len(y_true)

def levenshteinDistance(s1, s2):
    if len(s1) > len(s2):
        s1, s2 = s2, s1

    distances = range(len(s1) + 1)
    for i2, c2 in enumerate(s2):
        distances_ = [i2+1]
        for i1, c1 in enumerate(s1):
            if c1 == c2:
                distances_.append(distances[i1])
            else:
                distances_.append(1 + min((distances[i1], distances[i1 + 1], distances_[-1])))
        distances = distances_
    return distances[-1]

if __name__ == '__main__':
    # pred =['PP', 'UW', 'M', 'IY', 'N', 'JX', 'NN', 'TT', 'AA', 'KK', 'UW', 'K', 'IY', 'EY', 'R', 'WW', 'NN', 'K', 'AA', 'ZZ', 'IY', 'EH', 'ZZ', 'IY', 'T', 'AA', 'R', 'AA', 'Z', 'IY', 'CH', 'AA', 'N', 'WW', 'UW', 'PH', 'IY', 'K', 'WW', 'AA', 'R', 'IY', 'N', 'JX', 'NN', 'AA', 'TH', 'WW', 'K', 'AA', 'K', 'AA', 'N', 'IY', 'PH', 'UW', 'NX', 'S', 'AA', 'NX', 'HH', 'AA', 'K', 'IY', 'NN', 'N', 'NN', 'PP', 'WW', 'N', 'AA', 'KK', 'OW', 'P', 'AA', 'N', 'AA', 'MM', 'S', 'IY', 'JX', 'N', 'WW', 'NN', 'S', 'AA', 'NN', 'N', 'AA', 'NN', 'M', 'AA', 'IY', 'AX', 'TQ', 'T', 'AA']
    # true = ['P', 'OW', 'M', 'IY', 'M', 'JX', 'NN', 'PP', 'AX', 'KK', 'UW', 'K', 'IY', 'UW', 'R', 'WW', 'MM', 'K', 'WA', 'HH', 'AA', 'MM', 'KK', 'EH', 'Z', 'IY', 'NN', 'T', 'AA', 'L', 'R', 'EY', 'K', 'AA', 'Z', 'IY', 'CH', 'AX', 'N', 'WW', 'R', 'OW', 'PH', 'IY', 'K', 'OW', 'K', 'AA', 'WW', 'R', 'IY', 'M', 'JX', 'NN', 'T', 'AA', 'NN', 'PH', 'UW', 'NX', 'K', 'WA', 'K', 'AA', 'M', 'IY', 'PH', 'UW', 'NX', 'S', 'AX', 'NX', 'HH', 'AA', 'K', 'EH', 'IY', 'NX', 'N', 'WW', 'NN', 'M', 'UW', 'L', 'M', 'AA', 'L', 'KK', 'OW', 'P', 'AA', 'R', 'AA', 'MM', 'S', 'IY', 'WX', 'NN', 'HH', 'AA', 'NN', 'S', 'AA', 'NN', 'K', 'AA', 'NN', 'M', 'AA', 'WW', 'R', 'IY', 'AX', 'TQ', 'TT', 'AA']

    pred = ['IY', 'TT', 'WW', 'R', 'IY', 'R', 'AA', 'S', 'AA', 'AA', 'P', 'IY', 'ZZ', 'EH', 'T', 'S', 'IY', 'PH', 'IY', 'R', 'WW', 'K', 'AA', 'HH', 'OW', 'K', 'IY', 'T', 'EH', 'SS', 'IY', 'WW', 'N', 'AA', 'N', 'AA', 'K', 'UW', 'IY', 'N', 'IY', 'S', 'IY', 'PQ', 'SS', 'AA', 'R', 'N', 'AA', 'Z', 'IY', 'AA', 'TT', 'AA']
    true = ['UI', 'TQ', 'TT', 'OW', 'R', 'IY', 'R', 'WW', 'L', 'P', 'AX', 'S', 'AX', 'P', 'UW', 'CH', 'EY', 'T', 'EY', 'S', 'IY', 'NN', 'HH', 'UI', 'T', 'UW', 'L', 'R', 'AX', 'P', 'OW', 'K', 'IY', 'T', 'OW', 'HH', 'EY', 'SS', 'WW', 'N', 'AA', 'N', 'AX', 'K', 'UW', 'R', 'IY', 'N', 'WW', 'NN', 'S', 'UI', 'PQ', 'SS', 'AA', 'R', 'IY', 'N', 'AA', 'OW', 'Z', 'IY', 'AA', 'N', 'AA', 'TQ', 'TT', 'AA']
    print(ler(true, pred))
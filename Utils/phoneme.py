class Phoneme(object):

    def __init__(self, phoneme_path = 'Data/phoneme.txt'):
        self.phoneme_path = phoneme_path
        self.phone_encoding = self.get_phone_encoding()
        self.trans_dict = self.get_phone_transcript()

    def get_phone_encoding(self):
        f = open(self.phoneme_path, 'r')
        lines = f.readlines()
        f.close()

        phone_list = []

        for line in lines:
            line = line.strip()
            phone_list.append(line.split('\t')[0])
        return phone_list

    def transcript_Kog2p(self, phones):
        trans_phones = []
        for phone in phones:
            # print(phone)
            trans_phones.append(self.trans_dict[phone])

        return trans_phones

    def get_phone_transcript(self):
        transcript_path = self.phoneme_path[:-4] + '_transcript.txt'

        f = open(transcript_path, 'r')
        lines = f.readlines()
        f.close()

        trans_dict = {}

        for line in lines:
            line = line.strip()
            line_split = line.split('\t')
            # print('%s :/ %s' %(line_split[0], line_split[1]))
            trans_dict[line_split[0]] = line_split[1]

        # print(trans_dict)
        return trans_dict


if __name__ == '__main__':
    path = '../Data/phoneme.txt'
    # phone = Phoneme(phoneme_path=path)
    # print(len(phone.phone_encoding))
    # print(phone.phone_translate())
    # phone.get_phone()

    # print(Phoneme(path).transcript_Kog2p(['mm']))
    # print(Phoneme(path).trans_dict)
    p = Phoneme(path)
    # print(p.get_phone_transcript())
    print(p.trans_dict['wq'])



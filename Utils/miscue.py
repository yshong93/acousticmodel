
class Miscue(object):
    def __init__(self, miscue_path='Data/miscue.txt'):
        self.miscue_path = miscue_path
        self.miscue_encoding = self.get_phone_encoding()
        self.encoding = ['None', 'CUT', 'EXT', 'PAU', 'PHO', 'PRE', 'blank']


    def get_phone_encoding(self):
        f = open(self.miscue_path, 'r')
        lines = f.readlines()
        f.close()

        phone_list = []

        for line in lines:
            line = line.strip()
            phone_list.append(line.split('\t')[0])
        return phone_list
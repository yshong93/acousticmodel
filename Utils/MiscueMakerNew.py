import numpy as np
import textgrid
import os

import random

import librosa

from Utils.phoneme import Phoneme
from Utils.miscue import Miscue
import decimal
import math

class MiscueMaker(object):
    def __init__(self, hop_length, mel_one_width, sr=16000):
        self.nm = NoiseMaker()
        self.sr = sr
        self.tgm = self.TextGridManager()
        self.hop_length = hop_length
        self.mel_one_width = mel_one_width
        self.label_width = hop_length * mel_one_width

    def get_phones_label_list(self, interval_list):
        return [interval.mark for interval in interval_list]
    def phones_tier_to_ctc(self, interval_list, encoding):
        ctc_length = int(len(self.raw) / self.label_width)
        return self.tgm.interval_list_to_ctc(interval_list,
                                             self.label_width,
                                             ctc_length,
                                             encoding=encoding)

    def miscue_tier_to_ctc(self, raw, interval_list, encoding):
        ctc_length = math.ceil(int(len(raw) / self.hop_length) / self.mel_one_width)
        return self.tgm.interval_list_to_ctc(interval_list, self.label_width, ctc_length, encoding=encoding)


    def miscue_interval_to_ctc(self, raw, interval_list, mel_time, encoding):
        # ctc_length = math.ceil(int(len(raw) / mel_time)
        ctc_length = mel_time
        # label_width = int(len(raw) / mel_time)
        label_width =  self.label_width
        return self.tgm.interval_list_to_ctc(interval_list, label_width,
                                             ctc_length, encoding=encoding)

    def get_phones_list(self, textgrid_path):
        tg = textgrid.TextGrid()
        tg.read(textgrid_path)
        phones = tg.tiers[1]
        return [phone for phone in phones if phone.mark != 'sil']

    def generate_miscue_interval(self, miscue_intervals, min_time, max_time, mark):
        return_interval_list = []

        miscue_length = max_time - min_time
        for miscue_interval in miscue_intervals :
            if miscue_interval.minTime >= min_time:
                update_miscue_interval = textgrid.Interval(miscue_interval.minTime + miscue_length,
                                                           miscue_interval.maxTime + miscue_length,
                                                           miscue_interval.mark)
                return_interval_list.append(update_miscue_interval)
            else:
                return_interval_list.append(miscue_interval)

        return_interval = textgrid.Interval(min_time, max_time, mark)
        return_interval_list.append(return_interval)

        return sorted(return_interval_list, key=lambda interval: interval.minTime)


    def random_PAU(self, raw, miscue_interval_list):
        random_pick_phone = self.tgm.get_random_phoneme(miscue_interval_list)

        if random_pick_phone:
            rand_length_time = random.randrange(5, 12) / 10
            start_time = random_pick_phone.maxTime
            new_miscue_interval_list = self.generate_miscue_interval(miscue_interval_list,
                                                        start_time,
                                                        start_time+decimal.Decimal(rand_length_time),
                                                        'PAU')
            return_raw = self.nm.add_silence(raw, start_time, rand_length_time)
            return return_raw, new_miscue_interval_list

        else:
            return raw, miscue_interval_list


    def random_EXT(self, raw, miscue_interval_list):
        random_pick_phone = self.tgm.get_random_phoneme_vowel_phone(miscue_interval_list)

        if random_pick_phone:
            min_time = random_pick_phone.minTime
            max_time = random_pick_phone.maxTime
            phone_length = max_time - min_time

            [miscue_interval_list.remove(phone) for phone in miscue_interval_list if phone.minTime == min_time and phone.maxTime == max_time]

            rand_stretch_ratio = random.randrange(4, 6) / 10
            phone_length_stretch = phone_length * decimal.Decimal(rand_stretch_ratio)

            return_miscue_interval_list = self.generate_miscue_interval(miscue_interval_list,
                                                        min_time,
                                                        min_time + decimal.Decimal(phone_length_stretch),
                                                        'EXT')

            return_raw = self.nm.add_expand(raw,
                                            random_pick_phone.minTime,
                                            random_pick_phone.maxTime,
                                            rand_stretch_ratio)

            return return_raw, return_miscue_interval_list
        else:
            return raw, miscue_interval_list

    def random_PRE(self, raw, miscue_interval_list):
        random_pick_phone = self.tgm.get_random_phoneme_vowel_phone(miscue_interval_list)
        if random_pick_phone:
            sound_length = random_pick_phone.maxTime - random_pick_phone.minTime
            return_miscue_interval_list = self.generate_miscue_interval(
                                                        miscue_interval_list,
                                                        random_pick_phone.minTime,
                                                        random_pick_phone.maxTime + sound_length,
                                                        'PRE')

            return_raw = self.nm.add_duplicate(raw,
                                               random_pick_phone.minTime,
                                               random_pick_phone.minTime,
                                               random_pick_phone.maxTime)

            return return_raw, return_miscue_interval_list
        else:
            return raw, miscue_interval_list

    def null_miscue(self, raw, miscue_interval_list):
        return raw, miscue_interval_list

    class TextGridManager(object):
        def __init__(self):
            self.vowel_list = ['IY', 'EH', 'EY', 'AA', 'WW', 'UW', 'OW', 'AX', 'JH', 'JE', 'JA', 'JO', 'JU', 'JX', 'UI',
                               'WH', 'WE', 'OI', 'WA', 'WX', 'WI']

            self.pm = Phoneme()
            self.miscue = Miscue()

            # self.label_width =


        def get_random_phoneme(self, phones_list):
            real_phones_list = [phone for phone in phones_list if phone.mark != 'sil' or phone.mark in self.pm.phone_encoding]
            real_phones_list = real_phones_list[:-1]
            if real_phones_list:
                phone = random.choice(real_phones_list)
                return phone
            else:
                return None

        def get_random_phoneme_vowel(self, phones_list):
            vowel_list = [phone for phone in phones_list if phone.mark in self.vowel_list ]

            if vowel_list:
                phone = self.get_random_phoneme(vowel_list)
                if phone:
                    if phone.mark in self.vowel_list:
                        return_phone = phone
                        return return_phone

            return None


        def get_random_phoneme_vowel_phone(self, phones_list):
            vowel = self.get_random_phoneme_vowel(phones_list)

            if vowel:
                b_phone = [phone for phone in phones_list if phone.maxTime == vowel.minTime]
                if b_phone:
                    return_phone = textgrid.Interval(b_phone[0].minTime,
                                                     vowel.maxTime,
                                                     vowel.mark)
                    return return_phone
                else:
                    return None
            else:
                return None

        # def make_miscue_ctc(self, path, raw_len, miscue):

            # self.textgrid_to_ctc(path, ctc_len)

        def interval_list_to_ctc(self, interval_list, label_width, ctc_len, encoding):
            label_ctc = np.zeros((ctc_len+1,))
            # label_ctc.fill(0)
            for idx, phone in enumerate(interval_list):
                min_time = int(librosa.core.time_to_samples(phone.minTime, sr=16000))
                max_time = int(librosa.core.time_to_samples(phone.maxTime, sr=16000))

                min_idx = int(min_time / label_width)
                max_idx = int(max_time / label_width)

                if phone.mark == 'sil':
                    phone_encoding = encoding.index('SIL')
                else:
                    phone_encoding = encoding.index(phone.mark)

                # print('(%d, %d) / %d, %s' %(min_idx, max_idx, ctc_len, phone.mark))
                for i in range(min_idx, max_idx):
                    if i < ctc_len:
                        label_ctc[i] = phone_encoding

            return label_ctc[:-1]

        def textgrid_to_ctc(self, path, ctc_len):
            tg = textgrid.TextGrid()
            tg.read(path)

            label_width = 8*64
            phones1 = tg.tiers[1]

            encoding = self.pm.phone_encoding
            return self.interval_list_to_ctc(phones1, label_width, ctc_len, encoding=encoding)




class NoiseMaker(object):
    def __init__(self, sr=16000):
        self.sr = sr

    def phones_to_ctc(self, interval_list, encoding):
        ctc_length = int(len(self.raw) / self.label_width)
        return self.tgm.interval_list_to_ctc(interval_list, self.label_width, ctc_length, encoding=encoding)

    def add_silence(self, raw, start_time, length_time):
        start_samples = int(librosa.core.time_to_samples(start_time, self.sr))
        length_samples = int(librosa.core.time_to_samples(length_time, self.sr))

        return_raw = np.insert(raw, start_samples, np.zeros(length_samples))
        return_raw.flatten()
        return return_raw

    # @make_ui(sub_name='expand')
    def add_expand(self, raw, start_time, end_time, stretch_ratio):
        start_samples = int(librosa.core.time_to_samples(start_time, self.sr))
        end_samples = int(librosa.core.time_to_samples(end_time, self.sr))

        temp_slice = raw[start_samples:end_samples].copy()
        #         resample = self.slow(temp_slice, 16000)
        resample = librosa.effects.time_stretch(temp_slice, stretch_ratio)

        temp_raw = np.append(raw[:start_samples], resample)
        return_raw = np.append(temp_raw, raw[end_samples:])

        return return_raw

    # @make_ui(sub_name='dup')
    def add_duplicate(self, raw, insert_start_time, dup_start_time, dup_end_time):
        insert_start_samples = int(librosa.core.time_to_samples(insert_start_time, self.sr))
        dup_start_samples = int(librosa.core.time_to_samples(dup_start_time, self.sr))
        dup_end_samples = int(librosa.core.time_to_samples(dup_end_time, self.sr))

        sound_length_samples = dup_end_samples - dup_start_samples
        padding_samples = int(sound_length_samples / 2)

        dup_part = np.concatenate([np.zeros(padding_samples),raw[dup_start_samples:dup_end_samples],
                             np.zeros(padding_samples)])
        return_raw = np.insert(raw, insert_start_samples, dup_part)
        return return_raw

    def add_dissolve(self, start, end):
        length = end - start
        self.raw[start:end] = np.multiply(self.raw[start:end], np.linspace(1.0, 0.0, num=length))
        return self

    # @make_ui(sub_name='remove')
    def remove(self, start, end):
        length = end - start
        self.raw[start:end] = np.zeros(length)
        return self


    def slow(self, y, sr):
        D = librosa.stft(y, n_fft=2048, hop_length=512)
        D_slow = librosa.phase_vocoder(D, 1. / 2, hop_length=512)
        y_slow = librosa.istft(D_slow, hop_length=512)
        np.resize(y_slow, [1, sr])
        return y_slow



if __name__ == '__main__':
    test_path = '/media/yoonseok/Data/Corpus/Audio/korean_reading/audio/fv01/fv01_t01_s02.wav'
    test_label_path = '/media/yoonseok/Data/Corpus/Audio/korean_reading/label/fv01_t01_s03.textgrid'

    nm = NoiseMaker(test_path=test_path, test_label_path=test_label_path)
    print(nm.tgm.textgrid_to_ctc(test_label_path))
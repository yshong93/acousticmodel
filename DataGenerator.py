import os
import numpy as np
import collections
import random
import librosa
from AudioUtils import random_noise

from KoG2P.YSg2p import runKoG2P
from kolm.normalize import *


class DataReader(object):
    def __init__(self, wav_dir_path):
        self.wav_dir_path = wav_dir_path

    def get_dataset(self):
        numpy_list = collections.deque()
        for (path, dir_, files) in os.walk(self.wav_dir_path):
            numpy_list.extend([file for file in files if 'raw' in file])

        # random.shuffle(numpy_list)

        test_list = [numpy_list.pop()]
        numpy_list.pop()
        # test_list = ['raw_extract7.npy']

        train_list = numpy_list
        random.shuffle(train_list)
        # train_list = [numpy_list.pop() for i in range(5)]
        print(test_list)
        print(train_list)
        return train_list, test_list


#############################################################################
import math
from Utils.phoneme import Phoneme
from Utils.miscue import Miscue


class DataGenerator(object):
    def __init__(self, wav_dir_path, max_sample_count = 160000):
        self.wav_dir_path = wav_dir_path
        self.trainData, self.testData = DataReader(wav_dir_path).get_dataset()

        self.y_encoding_error = collections.defaultdict(int)

        self.phone_manager = Phoneme()
        self.y_encoding = self.phone_manager.get_phone_encoding()

        self.miscue_manager = Miscue()

        self.max_sample_count = max_sample_count

        self.mel_max_width = 2500
        self.mel_one_width = 8
        self.mel_max_num = math.ceil(self.mel_max_width / self.mel_one_width)
        self.mel_height = 40

        self.hop_length = 64

        self.phone_max_len = 300
        self.mel_shape = (None, self.mel_max_width, self.mel_height, 1)


    def feature_extract_x(self, raw):
        # Mel Spectrogram
        mel = librosa.power_to_db(librosa.feature.melspectrogram(raw, sr=16000, n_mels=self.mel_height, hop_length=64))

        # MFCCs
        # mel = librosa.feature.mfcc(y=raw, sr=16000, n_mfcc=self.mel_height, hop_length=64)

        # d_mel = librosa.feature.delta(mel)
        # d_d_mel = librosa.feature.delta(d_mel)

        total_mel_feature = np.stack([mel.transpose()], axis=-1)

        last = total_mel_feature.shape[0]
        total_slice_list = np.split(total_mel_feature, range(self.mel_one_width, last, self.mel_one_width), axis=0)
        return np.stack(total_slice_list[:-1], axis=0)

    def feature_extract_y(self, phones):
        phone_list = phones.split(' ')
        phone_list = self.phone_manager.transcript_Kog2p(phone_list)
        phone_encoding_list = []
        # phone_encoding_list.append(0)
        for idx, phone in enumerate(phone_list):
            #         one_hot = np.zeros((1, len(y_encoding)))
            if phone in self.y_encoding:

                phone_encoding_list.append(self.y_encoding.index(phone))
            else:
                phone_encoding_list.append(0)
                self.y_encoding_error[phone] = self.y_encoding_error[phone] + 1

        # phone_encoding_list.append(0)
        return np.stack(phone_encoding_list)

    def sentence_to_phones(self, sentence):
        sentence = bySentence(sentence)
        phones = runKoG2P(sentence)
        return phones
        #################################################################################################

    def get_batch(self, data_list, size):
        size = len(data_list)

        # math.ceil(self.mel_max_width / self.mel_one_width)
        batch_max_raw_len = max([len(data['raw']) for data in data_list])
        mel_width_max_batch = math.ceil(int(batch_max_raw_len / self.hop_length) / self.mel_one_width)
        # phones_max_len_batch = int(max([for data in data_list]))

        X_data = np.zeros([size, mel_width_max_batch, self.mel_one_width, self.mel_height, 1])
        labels = np.zeros([size, self.phone_max_len])
        input_length = np.zeros([size, 1])
        label_length = np.zeros([size, 1])

        for idx, data in enumerate(data_list):
            raw = data['raw']
            raw = librosa.util.normalize(raw)
            raw = random_noise(raw)
            mel = self.feature_extract_x(raw)
            mel_time = mel.shape[0]
            #             print(mel.shape)
            #         mel = mel_list[idx]
            X_data[idx, :mel_time, :, :, :] = mel
            scrt = data['scrt']
            phones = self.sentence_to_phones(scrt)
            phones_list = self.feature_extract_y(phones=phones)
            phones_length = phones_list.shape[0]
            labels[idx, :phones_length] = phones_list

            input_length[idx] = mel_time
            label_length[idx] = phones_length

        inputs = {'the_input': X_data,
                  'the_labels': labels,
                  'input_length': input_length,
                  'label_length': label_length
                  }
        outputs = {'ctc': np.zeros([size]),
                   'decoder': np.zeros([size])}  # dummy data for dummy loss function
#                   'decoder': labels}  # dummy data for dummy loss function
        # print(X_data.shape)
        return (inputs, outputs)

    def train_generator(self, batchSize):
        batch_data_list = []
        while True:
            for numpy_name in self.trainData:
                data_list = np.load(self.wav_dir_path + numpy_name)

                for data in data_list:
                    if len(data['raw']) <= self.max_sample_count:
                        batch_data_list.append(data)
                    if len(batch_data_list) == batchSize:
                        #                         print('train_gen')
                        yield self.get_batch(batch_data_list, batchSize)
                        batch_data_list = []

    def test_generator(self, batchSize):
        batch_data_list = []
        for numpy_name in self.trainData:
            # for numpy_name in self.testData:
            data_list = np.load(self.wav_dir_path + numpy_name)

            while True:
                for data in data_list:
                    if len(data['raw']) <= self.max_sample_count:
                        batch_data_list.append(data)
                    if len(batch_data_list) == batchSize:
                        yield self.get_batch(batch_data_list, batchSize)
                        batch_data_list = []

    def phone_Test_Generator(self, batchSize):
        batch_data_list = []
        while True:
            for numpy_name in self.testData:
                data_list = np.load(self.wav_dir_path + numpy_name)

                for data in data_list:
                    if len(data['raw']) <= self.max_sample_count:
                        batch_data_list.append(data['scrt'])
                    if len(batch_data_list) == batchSize:
                        yield batch_data_list
                        batch_data_list = []

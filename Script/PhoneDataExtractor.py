import librosa
import os
import collections
import math
import numpy as np
import random
from multiprocessing import Pool
import datetime

from Script.ScriptExtractor import read_script_sentence, get_script_from_filename

import  slackweb

slack_url = 'https://hooks.slack.com/services/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'
slack= slackweb.Slack(url=slack_url)



def get_dataset(dir_path):
    numpy_list = collections.deque()
    for (path, dir_, files) in os.walk(wav_dir_path):
        for file_name in files:
            ext = os.path.splitext(file_name)[-1]
            if ext == '.WAV':
                file = "%s/%s" % (path, file_name)
                numpy_list.append(file)

    return numpy_list


def get_wav(idx, file_path):
    if (idx % 1000 == 0): print(idx)
    # wav_path = file_path
    raw, sr = librosa.load(file_path, sr=16000)

    phn_path = file_path.split('.')[0] + '.PHN'
    phone_raw = collections.deque()

    with open(phn_path, 'r') as f:
        lines = f.readlines()
        for line in lines:
            line = line.strip()
            token = line.split(' ')
            tokend_raw = raw[int(token[0]):int(token[1])].copy()
            phone_raw.append((tokend_raw, token[2]))
    return phone_raw


if __name__ == '__main__':
    start_string = 'Feature Extraction Start : ' + str(datetime.datetime.now())
    slack.notify(text=start_string, channel="#notification", username='Acoustic Bot!!', icon_emoji=": coffee")

    # save_dir_path =

    wav_dir_path = '/media/yoonseok/Data/Corpus/Audio/TIMIT/data/TRAIN/'
    data_list = get_dataset(wav_dir_path)


    # pool = Pool(16)
    start_time = datetime.datetime.now()

    now_time = datetime.datetime.now()
    print('%d 파일 생성을 시작합니다. %s' % ( len(data_list), now_time))

    phone_data = collections.deque()
    for idx, data_path in enumerate(data_list):
        phone_raw = get_wav(idx, data_path)
        phone_data.extend(phone_raw)

    # results = pool.starmap(get_wav, enumerate(data_list))
    # results = pool.map(get_wav, enumerate(data_list))
    # results = list(filter(None.__ne__, results))
    print(len(phone_data))
    np.save(wav_dir_path + 'feature/raw_extract.npy', phone_data)
    # print('%d / %d 파일 생성 완료!. %s' % (i, len(save_slice_wav_list), datetime.datetime.now() - now_time))

    print('\t ## 파일 생성 완료! %s ' % (datetime.datetime.now() - start_time))

    end_string = 'Feature Extraction End : ' + str(datetime.datetime.now())
    slack.notify(text=end_string, channel="#notification", username='Acoustic Bot!!', icon_emoji=": coffee")
    # raw, sr = librosa.load(wav_dir_path + wav_list[0])

    # np.save(wav_dir_path + 'feature/raw_extractTest.npy', raw)
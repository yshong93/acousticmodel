import librosa
import os
import collections
import math
import numpy as np
from KoG2P.YSg2p import runKoG2P
from kolm.normalize import bySentence

from Script.ScriptExtractor import get_script_from_filename

import  slackweb

slack_url = 'https://hooks.slack.com/services/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'
slack= slackweb.Slack(url=slack_url)


def find_subfiles(path, file_extension):
    name_list = collections.deque()
    for (path, dir_, files) in os.walk(path):
        #         print(len(path))
        #         print(path)
        #         print(dir_)
        sub_folder_name = path.split('/')[-1]
        #         print(sub_folder_name)
        for file_name in files:
            ext = os.path.splitext(file_name)[-1]
            if ext == file_extension:
                file = "%s/%s" % (sub_folder_name, file_name)

                name_list.append(file)

    return name_list


def list_slicer(my_list, slice_num):
    iter_num = math.ceil(len(my_list) / slice_num)
    print(iter_num)
    slice_list = []
    for i in range(iter_num):
        slice_list.append([])

    for idx, path in enumerate(my_list):
        column = int(idx % iter_num)
        print('%d , %d' %(idx, column))

        slice_list[column].append(path)

    return slice_list

from multiprocessing import Pool
import datetime

raw_max_len = 0
phones_max_len = 0

def read_wav(path):
    try:
        return_dict = {}
        return_dict['path'] = path
        return_dict['scrt'] = get_script_from_filename(path)

        return return_dict
    except Exception as ex:
        print('# Error : %s (%s)' %(path, ex))
        return


if __name__ == '__main__':
    start_string = 'Feature Extraction Start : ' + str(datetime.datetime.now())
    slack.notify(text=start_string, channel="#notification", username='Acoustic Bot!!', icon_emoji=": coffee")
    wav_dir_path = '/media/yoonseok/Data/Corpus/Audio/korean_reading/'

    # save_dir_path =

    wav_list = find_subfiles(wav_dir_path + 'audio/', '.wav')

    save_slice_wav_list = list_slicer(wav_list, 1024 * 4)

    pool = Pool(16)
    start_time = datetime.datetime.now()
    for i in range(len(save_slice_wav_list)):
        now_time = datetime.datetime.now()
        print('%d / %d 파일 생성을 시작합니다. %s' % (i, len(save_slice_wav_list), now_time))

        # with open('file.txt') as source_file:
        # chunk the work into batches of 4 lines at a time
        results = pool.map(read_wav, save_slice_wav_list[i])
        results = list(filter(None.__ne__, results))


        # print(len(results))
        np.save(wav_dir_path + 'align_script/alignerScript' + str(i) + '.npy', results)
        print('%d / %d 파일 생성 완료!. %s' % (i, len(save_slice_wav_list), datetime.datetime.now() - now_time))

    print('\t ## 파일 생성 완료! %s ' % (datetime.datetime.now() - start_time))

    end_string = 'Feature Extraction End : ' + str(datetime.datetime.now())
    slack.notify(text=end_string, channel="#notification", username='Acoustic Bot!!', icon_emoji=": coffee")
    # raw, sr = librosa.load(wav_dir_path + wav_list[0])

    # np.save(wav_dir_path + 'feature/raw_extractTest.npy', raw)
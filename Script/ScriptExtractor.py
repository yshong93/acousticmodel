# data structure
# {
#     // wav 신호
#     y:[ '0'],

#     // 파일 이름
#     fp:'mv01_t01_s01.wav',
#
#     // 스크립트
#     scrt:'기차도 전기도 없었다.'
# }
import re

class ScriptExtractor(object):
    def __init__(self, path = 'Data/', file_name = 'script_nmbd_by_sentence.txt'):
        self.scripts = self.read_script_sentence(path + file_name)

    def read_script_sentence(self, file_path):
        return_dict = {}
        with open(file_path, encoding='utf-16') as file:
            lines = file.readlines()

            regex = re.compile(r'<\d+.*>')

            story_idx = -1
            for idx, val in enumerate(lines):
                if (not val.isspace()):
                    sentence = val.strip()

                    if (regex.findall(sentence)):
                        re_test = re.compile('\d+[.]')
                        story_idx = re_test.search(sentence).group()
                        story_idx = int(story_idx[:-1])
                        #                     story_idx = story_idx + 1

                        story_dict = {}
                        return_dict[story_idx] = story_dict


                    else:
                        hangul = re.compile('[^ ㄱ-ㅣ가-힣|0-9]+')  # 한글과 띄어쓰기를 제외한 모든 글자

                        sentence_split = sentence.split('.')

                        content = '.'.join(sentence_split[1:]).strip()
                        return_dict[story_idx][int(sentence_split[0])] = hangul.sub('', content)

                        # pp.pprint(return_dict)
        return return_dict

    def get_script_from_filename(self, file_name):
        if('n' in file_name):
            # file_name = 'fx08/nfy_s06_t07_s12.wav'
            info = file_name.split('.')[0].split('/')[1]
            split_info = info.split('_')

            doc_num = split_info[2][1:]
            #     print(doc_num)

            sent_num = split_info[3][1:]

            return self.scripts[int(doc_num)][int(sent_num)]
        else:
            #     file_name = 'fv17/fv17_t03_s18.wav'
            info = file_name.split('.')[0].split('/')[1]
            split_info = info.split('_')

            doc_num = split_info[1][1:]
            sent_num = split_info[2][1:]

            return self.scripts[int(doc_num)][int(sent_num)]


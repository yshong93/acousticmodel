import keras
from keras.layers import Input
from keras.layers import GaussianNoise
from keras.layers import TimeDistributed
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers import Masking
from keras.layers import Bidirectional, SimpleRNN
from keras.layers import Lambda
from keras.layers import Dropout
from keras.layers import merge, Activation

from keras.activations import relu

from keras.regularizers import l1, l2


def brsmv1(x, num_features=39, num_classes=28, num_hiddens=256, num_layers=5,
           dropout=0.2, zoneout=0.0, input_dropout=False,
           input_std_noise=.0, weight_decay=1e-4, residual=None,
           layer_norm=None, mi=None, activation='tanh'):
    """ BRSM v1.0
    Improved features:
        * Residual connection
        * Variational Dropout
        * Zoneout
        * Layer Normalization
        * Multiplicative Integration
    Note:
        Dropout, zoneout and weight decay is tied through layers, in order to
        minimizing the number of hyper parameters
    Reference:
        [1] Gal, Y, "A Theoretically Grounded Application of Dropout in
        Recurrent Neural Networks", 2015.
        [2] Graves, Alex, Abdel-rahman Mohamed, and Geoffrey Hinton. "Speech
        recognition with deep recurrent neural networks", 2013.
        [3] Krueger, David, et al. "Zoneout: Regularizing rnns by randomly
        preserving hidden activations", 2016.
        [4] Ba, Jimmy Lei, Jamie Ryan Kiros, and Geoffrey E. Hinton. "Layer
        normalization.", 2016.
        [5] Wu, Yuhuai, et al. "On multiplicative integration with recurrent
        neural networks." Advances In Neural Information Processing Systems.
        2016.
        [6] Wu, Yonghui, et al. "Google's Neural Machine Translation System:
        Bridging the Gap between Human and Machine Translation.", 2016.
    """

#    x = Input(name='inputs', shape=(None, num_features))
    o = x

    if input_std_noise is not None:
        o = GaussianNoise(input_std_noise)(o)

    if residual is not None:
        o = TimeDistributed(Dense(num_hiddens*2,
                                  W_regularizer=l2(weight_decay)))(o)

    if input_dropout:
        o = Dropout(dropout)(o)

        
    for i, _ in enumerate(range(num_layers)):
        new_o = Bidirectional(LSTM(num_hiddens,
                                   return_sequences=True,
                                   W_regularizer=l2(weight_decay),
                                   U_regularizer=l2(weight_decay),
                                   dropout_W=dropout,
                                   dropout_U=dropout,
                                   #zoneout_c=0,
                                   #zoneout_h=0,
                                   activation='tanh'))(o)

        if residual is not None:
            o = merge([new_o,  o], mode=residual)
        else:
            o = new_o

    o = TimeDistributed(Dense(num_classes,
                              W_regularizer=l2(weight_decay)))(o)
    
    return (x, o)


def maas(x, ):
    """ Maas' model.
    Reference:
        [1] Maas, Andrew L., et al. "Lexicon-Free Conversational Speech
        Recognition with Neural Networks." HLT-NAACL. 2015.
    """

    #x = Input(name='inputs', shape=(None, num_features))
    num_features=81
    num_classes=29
    num_hiddens=1824
    dropout=0.1
    max_value=20
        
    o = x

    def clipped_relu(x):
        return relu(x, max_value=max_value)

    o = TimeDistributed(keras.layers.Reshape((1, -1)))(o)
        
    # First layer
    o = TimeDistributed(Dense(num_hiddens))(o)
    o = TimeDistributed(Activation(clipped_relu))(o)

    # Second layer
    o = TimeDistributed(Dense(num_hiddens))(o)
    o = TimeDistributed(Activation(clipped_relu))(o)
    
    o = TimeDistributed(keras.layers.Reshape((-1,)))(o)

    # Third layer
    o = Bidirectional(SimpleRNN(num_hiddens, return_sequences=True,
                                dropout_W=dropout,
                                activation='relu',
                                init='he_normal'), merge_mode='sum')(o)

    o = TimeDistributed(keras.layers.Reshape((1, -1)))(o)
    # Fourth layer
    o = TimeDistributed(Dense(num_hiddens, activation=clipped_relu))(o)


    # Fifth layer
    o = TimeDistributed(Dense(num_hiddens, activation=clipped_relu))(o)


    # Output layer
    o = TimeDistributed(Dense(num_classes))(o)

    return (x, o)
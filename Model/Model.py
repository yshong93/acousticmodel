import Keras

input_data = Input(shape=(None, data_gen.mel_one_width, data_gen.mel_height, 1), name='the_input')

fc_size = 1024
# First 3 FC layers
init = random_normal(stddev=0.046875)

conv1 = TimeDistributed(Conv2D(filters=64, kernel_size=(1, 3), activation='relu'))(input_data)
conv1 = TimeDistributed(Dropout(0.8))(conv1)
conv2 = TimeDistributed(Conv2D(filters=64, kernel_size=(1, 3), activation='relu'))(conv1)
conv2 = TimeDistributed(Dropout(0.8))(conv2)
conv3 = TimeDistributed(Conv2D(filters=64, kernel_size=(1, 3), activation='relu'))(conv2)
maxpool = TimeDistributed(keras.layers.MaxPooling2D(pool_size=((1, 2))))(conv3)
maxpool = TimeDistributed(Dropout(0.8))(maxpool)

conv1 = TimeDistributed(Conv2D(filters=64, kernel_size=(1, 3), activation='relu'))(maxpool)
conv1 = TimeDistributed(Dropout(0.8))(conv1)
conv2 = TimeDistributed(Conv2D(filters=64, kernel_size=(1, 3), activation='relu'))(conv1)
conv2 = TimeDistributed(Dropout(0.8))(conv2)
conv3 = TimeDistributed(Conv2D(filters=64, kernel_size=(1, 3), activation='relu'))(conv2)
maxpool = TimeDistributed(keras.layers.MaxPooling2D(pool_size=((1, 2))))(conv3)
maxpool = TimeDistributed(Dropout(0.8))(maxpool)

reshape = TimeDistributed(keras.layers.Reshape((1, -1)))(maxpool)  # (merge)
dense1 = TimeDistributed(Dense(units=fc_size, activation='relu', name="dense_1", kernel_initializer='glorot_uniform'))(
    reshape)
dense1 = TimeDistributed(Dropout(0.8))(dense1)

dense2 = TimeDistributed(Dense(units=fc_size, activation='relu', name="dense_2", kernel_initializer='glorot_uniform'))(
    dense1)
dense1 = TimeDistributed(Dropout(0.2))(dense1)
dense2 = TimeDistributed(Dense(units=fc_size, activation='relu', name="dense_2", kernel_initializer='glorot_uniform'))(
    dense1)

# Layer 5+6 Time Dist Dense Layer & Softmax
x = TimeDistributed(Dense(fc_size, activation='relu', kernel_initializer=init, bias_initializer=init))(dense2)
# x = TimeDistributed(Dropout(0.8))(x)
# y_pred = TimeDistributed(Dense(len(data_gen.y_encoding), name="y_pred", kernel_initializer=init,
#                                bias_initializer=init, activation="softmax"), name="out")(x)
output = TimeDistributed(Dense(len(data_gen.y_encoding), name="y_pred", kernel_initializer=init,
                               bias_initializer=init, activation="softmax"), name="out")(x)

labels = Input(name='the_labels', shape=[data_gen.phone_max_len], dtype='float32')
input_length = Input(name='input_length', shape=[1], dtype='int64')
label_length = Input(name='label_length', shape=[1], dtype='int64')

# Keras doesn't currently support loss funcs with extra parameters
# so CTC loss is implemented in a lambda layer
loss_out = Lambda(ctc_lambda_func, output_shape=(1,), name='ctc')([output, labels, input_length, label_length])


y_pred = Lambda(ctc_decode, output_shape=decode_output_shape, name='decoder')([output, input_length])
#              arguments={'is_greedy': True},

# out = Lambda(ctc_decode, output_shape=(300,), name='ctc_out')([y_pred, input_length])
adam = keras.optimizers.Adam(lr=0.00001)

# accuracy_ctc = Lambda(ctc_accuracy_func, )

# y_ctc_pred = K.ctc_label_dense_to_sparse(y_pred, data_gen.mel_max_num)

# model.compile(loss="categorical_crossentropy", optimizer=adam , metrics = [metrics.categorical_accuracy])

model = Model(inputs=[input_data, labels, input_length, label_length], outputs=[loss_out, y_pred])
model.compile(loss={'ctc': lambda y_true, y_pred: y_pred, 'decoder':decoder_dummy_loss},
              optimizer=adam, metrics={'decoder': 'accuracy'})
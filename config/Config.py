import os
import collections
import random

def get_data_feature_set(self):
    numpy_list = collections.deque()
    for (path, dir_, files) in os.walk(self.wav_dir_path):
        numpy_list.extend([file for file in files if 'raw' in file])

    # random.shuffle(numpy_list)

    test_list = [numpy_list.pop()]
    numpy_list.pop()
    # test_list = ['raw_extract7.npy']

    train_list = numpy_list
    random.shuffle(train_list)
    # train_list = [numpy_list.pop() for i in range(5)]
    print(test_list)
    print(train_list)
    return train_list, test_list
# import ./Config
import json
import os

with open('./config/DeepCNN_config.json', 'r') as f:
    PhonemeConfig = json.load(f)

phoneme_classifier_config = {
    "MODEL_PATH" : os.path.join(PhonemeConfig["PATH"]["MODEL_DIR"], PhonemeConfig["MODEL_NAME"]),
    "TENSORBOARD_DIR" : os.path.join(PhonemeConfig["PATH"]["TENSORBOARD_DIR"], PhonemeConfig["MODEL_NAME"]),
    "RAW_FEATURE_DIR" : PhonemeConfig["PATH"]["RAW_FEATURE_DIR"],

    "TRAIN" : PhonemeConfig["TRAIN"]
}
import os
# os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
# os.environ["CUDA_VISIBLE_DEVICES"]="1"

TENSORBOARD_DIR = '/media/yoonseok/Data/TensorBoard/'+\
                  'RecitePhoneme/DeepCNN+biLSTM-1'
wav_dir_path = '/media/yoonseok/Data/Corpus/Audio/korean_reading/feature5/'
model_save_path = '/media/yoonseok/Data/ModelBackup/DeepCNN/DeepCNN_LSTM_1.h5'

from DataGenerator import DataGenerator
from CTCUtils import *
from Utils.phoneme import Phoneme

from Model.Metric import batch_WER, wer
from keras.callbacks import ModelCheckpoint, ReduceLROnPlateau

import keras
from keras.layers import Input, Conv2D, Dense, Dropout, LSTM, TimeDistributed, Lambda, Embedding, Activation, \
    MaxPooling2D, Bidirectional, Conv1D, RNN
# from keras.metrics import binary_accuracy
from keras.models import Model
from keras.initializers import random_normal
from keras.utils import multi_gpu_model
from keras.activations import relu

from keras import Model
from keras.utils import multi_gpu_model


class ModelMGPU(Model):
    def __init__(self, ser_model, gpus):
        pmodel = multi_gpu_model(ser_model, gpus)
        self.__dict__.update(pmodel.__dict__)
        self._smodel = ser_model

    def __getattribute__(self, attrname):
        '''Override load and save methods to be used from the serial-model. The
        serial-model holds references to the weights in the multi-gpu model.
        '''
        # return Model.__getattribute__(self, attrname)
        if 'load' in attrname or 'save' in attrname:
            return getattr(self._smodel, attrname)

        return super(ModelMGPU, self).__getattribute__(attrname)

data_gen = DataGenerator(wav_dir_path)
# print(Phoneme().get_phone_encoding())
data_gen.y_encoding = Phoneme().get_phone_encoding()
filter_size = 512

def clipped_relu(x):
    return relu(x, max_value=20)

with tf.device('/cpu:0'):
    input_data = Input(shape=(None, data_gen.mel_one_width, data_gen.mel_height, 1), name='the_input')

    fc_size = 2048
    # First 3 FC layers
    init = random_normal(stddev=0.046875)

    conv1 = TimeDistributed(Conv2D(filters=filter_size, kernel_size=(8, 5), activation=clipped_relu))(input_data)
    conv1 = TimeDistributed(Dropout(0.2))(conv1)
    conv1 = TimeDistributed(keras.layers.MaxPooling2D(pool_size=((1, 2))))(conv1)

    conv2 = TimeDistributed(Conv2D(filters=filter_size, kernel_size=(1, 5), activation=clipped_relu))(conv1)
    conv2 = TimeDistributed(Dropout(0.2))(conv2)
    conv2 = TimeDistributed(keras.layers.MaxPooling2D(pool_size=((1, 2))))(conv2)

    conv3 = TimeDistributed(Conv2D(filters=filter_size, kernel_size=(1, 5), activation=clipped_relu))(conv2)
    conv3 = TimeDistributed(Dropout(0.2))(conv3)

    reshape = TimeDistributed(keras.layers.Reshape((1, -1)))(conv3)  # (merge)
    dense1 = TimeDistributed(Dense(units=fc_size, activation=clipped_relu, name="dense_1", kernel_initializer='glorot_uniform'))(
        reshape)
    dense1 = TimeDistributed(Dropout(0.2))(dense1)

    dense2 = TimeDistributed(Dense(units=fc_size, activation=clipped_relu, name="dense_2", kernel_initializer='glorot_uniform'))(
        dense1)
    dense2 = TimeDistributed(Dropout(0.2))(dense2)

    x = TimeDistributed(Dense(fc_size, activation=clipped_relu, kernel_initializer=init, bias_initializer=init))(dense2)
    x = TimeDistributed(Dropout(0.2))(x)

    # Layer 4 BiDirectional RNN
    x = TimeDistributed(Bidirectional(LSTM(512, return_sequences=True, activation='relu', dropout=0.2,
                           kernel_initializer='he_normal', name='birnn'), merge_mode='sum'))(x)

    x = TimeDistributed(Dropout(0.2))(x)

    # y_pred = TimeDistributed(Dense(len(data_gen.y_encoding), name="y_pred", kernel_initializer=init,
    #                                bias_initializer=init, activation="softmax"), name="out")(x)
    output = TimeDistributed(Dense(len(data_gen.y_encoding), name="y_pred", kernel_initializer=init,
                                   bias_initializer=init, activation="softmax"), name="out")(x)

    labels = Input(name='the_labels', shape=[data_gen.phone_max_len], dtype='float32')
    input_length = Input(name='input_length', shape=[1], dtype='int64')
    label_length = Input(name='label_length', shape=[1], dtype='int64')

    # Keras doesn't currently support loss funcs with extra parameters
    # so CTC loss is implemented in a lambda layer
    loss_out = Lambda(ctc_lambda_func, output_shape=(1,), name='ctc')([output, labels, input_length, label_length])


    y_pred = Lambda(ctc_decode, output_shape=decode_output_shape, name='decoder')([output, labels, input_length, label_length])
    #              arguments={'is_greedy': True},

    # out = Lambda(ctc_decode, output_shape=(300,), name='ctc_out')([y_pred, input_length])
    adam = keras.optimizers.Adam(lr=0.0001)

    # accuracy_ctc = Lambda(ctc_accuracy_func, )

    # y_ctc_pred = K.ctc_label_dense_to_sparse(y_pred, data_gen.mel_max_num)

    # model.compile(loss="categorical_crossentropy", optimizer=adam , metrics = [metrics.categorical_accuracy])

    model = Model(inputs=[input_data, labels, input_length, label_length], outputs=[loss_out, y_pred, output])

# parallel_model = multi_gpu_model(model, gpus=2)
parallel_model = ModelMGPU(model , 2)

parallel_model.compile(loss={'ctc': ctc_dummy_loss, 'decoder':decoder_dummy_loss}, optimizer=adam)
# model.compile(loss={'ctc': lambda y_true, y_pred: y_pred}, optimizer=adam, metrics={'dense_3': 'accuracy'})

checkpoint = ModelCheckpoint(model_save_path,
                             monitor='val_ctc_loss', verbose=1, mode='max')
reduce_lr= ReduceLROnPlateau(monitor='val_ctc_loss', factor=0.1, patience=3, verbose=1, mode='auto',
                             epsilon=0.001, cooldown=0)
# stop  = EarlyStopping(monitor='val_loss', min_delta=0, patience=5, verbose=1, mode='auto')
tb_callback = keras.callbacks.TensorBoard(log_dir=TENSORBOARD_DIR, histogram_freq=0, write_graph=True)
print(parallel_model.summary())


hist_current = parallel_model.fit_generator(data_gen.train_generator(24),
                                   steps_per_epoch=500,
                                   epochs=60,
                                   # epochs=1000,
                                   verbose=1,
                                   validation_data=data_gen.test_generator(24),
#                                    validation_steps=1024,
                                   validation_steps=324, # 7767
                                   shuffle=False,
                                   callbacks=[tb_callback, checkpoint, reduce_lr],
                                   initial_epoch=0)

# model.save('/media/yoonseok/Data/ModelBackup/DeepCNN/1_CNN_model.h5')

import Utils.slack
Utils.slack.send_messeage('학습 끝. 빨리 확인하시요.')
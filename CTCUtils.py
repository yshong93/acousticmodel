import keras.backend as K
import tensorflow as tf
from tensorflow.python.ops import ctc_ops as ctc

def ctc_lambda_func_K(args):
    y_pred, labels, input_length, label_length = args
    # the 2 is critical here since the first couple outputs of the RNN
    # tend to be garbage:
    # y_pred = y_pred[:, 2:, :, :]

    # result = y_pred[:, :, 0, :]
    result = K.squeeze(y_pred, axis=2)
    result = result[:, 2:, :]

    #     label_length = tf.to_int32(tf.squeeze(label_length, axis=1))
    #     input_length = tf.to_int32(tf.squeeze(input_length, axis=1))

    # print('* labe : %s, result : %s, input_length : %s, label_length : %s' % (
    #     labels.shape, result.shape, input_length.shape, label_length.shape))
    return K.ctc_batch_cost(labels, result, input_length, label_length)

def ctc_lambda_func(args):
    y_pred, labels, input_length, label_length = args
    # the 2 is critical here since the first couple outputs of the RNN
    # tend to be garbage:
    #     y_pred = y_pred[:, 2:, :]

    result = K.squeeze(y_pred, axis=2)
    # result = result[:, 2:, :]

    #     label_length = tf.to_int32(tf.squeeze(label_length, axis=1))
    #     input_length = tf.to_int32(tf.squeeze(input_length, axis=1))

    # print('* labe : %s, result : %s, input_length : %s, label_length : %s' % (
    #     labels.shape, result.shape, input_length.shape, label_length.shape))
    # return K.ctc_batch_cost(labels, result, input_length, label_length)
    return ys_ctc_batch_cost(labels, result, input_length, label_length)


def ys_ctc_batch_cost(y_true, y_pred, input_length, label_length):
    """Runs CTC loss algorithm on each batch element.

    # Arguments
        y_true: tensor `(samples, max_string_length)`
            containing the truth labels.
        y_pred: tensor `(samples, time_steps, num_categories)`
            containing the prediction, or output of the softmax.
        input_length: tensor `(samples, 1)` containing the sequence length for
            each batch item in `y_pred`.
        label_length: tensor `(samples, 1)` containing the sequence length for
            each batch item in `y_true`.

    # Returns
        Tensor with shape (samples,1) containing the
            CTC loss of each element.
    """
    label_length = tf.to_int32(tf.squeeze(label_length))
    input_length = tf.to_int32(tf.squeeze(input_length))
    sparse_labels = tf.to_int32(K.ctc_label_dense_to_sparse(y_true, label_length))

    y_pred = tf.log(tf.transpose(y_pred, perm=[1, 0, 2]) + 1e-8)

    return tf.expand_dims(ctc.ctc_loss(inputs=y_pred,
                                       labels=sparse_labels,
                                       sequence_length=input_length,
                                       ignore_longer_outputs_than_inputs=True), 1)


def ctc_decode(args):
    y_pred, labels, input_length, label_length = args
    # y_pred, input_length = args
    # print('y_pred : %s, input_length : %s' % (y_pred.shape, input_length.shape))
    y_pred = y_pred[:, :, 0, :]
    input_length = input_length[:, 0]
    #     input_length = tf.squeeze(input_length)
    # print('y_pred : %s, input_length : %s' % (y_pred.shape, input_length.shape))
    dense, _ = K.ctc_decode(y_pred, input_length)
    # print('dense : %s' %dense[0].shape)
    return dense


def decode_output_shape(args):
    y_pred, labels, input_length, label_length = args
    # y_pred_shape, seq_len_shape = inputs_shape
    # print('decode_output_shape : %s' % y_pred_shape[:1])
    return (y_pred[:1], None)
    # return [y_pred_shape[:1], None]

def decoder_dummy_loss(y_true, y_pred):
    """ Little hack to make CTC working with Keras
    """
    return K.zeros((1,))

def ctc_dummy_loss(y_true, y_pred):
    return y_pred

def ler(y_true, y_pred, **kwargs):
    """
        Label Error Rate. For more information see 'tf.edit_distance'
    """
    return tf.reduce_mean(tf.edit_distance(y_pred, y_true, **kwargs))

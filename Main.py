import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="0"

TENSORBOARD_DIR = '/media/yoonseok/Data/TensorBoard/'+\
                  'ReciteCTC-feature/3'
wav_dir_path = '/media/yoonseok/Data/Corpus/Audio/korean_reading/feature4/'

from DataGenerator import DataGenerator
from CTCUtils import *

from Model.Metric import f1_score

import keras
from keras.layers import Input, Conv2D, Dense, Dropout, LSTM, TimeDistributed, Lambda, Embedding, Activation, \
    MaxPooling2D, Bidirectional, Conv1D, RNN
from keras.models import Model
from keras.initializers import random_normal


data_gen = DataGenerator(wav_dir_path)
filter_size = 1024

input_data = Input(shape=(None, data_gen.mel_one_width, data_gen.mel_height, 1), name='the_input')

fc_size = 1024
# First 3 FC layers
init = random_normal(stddev=0.046875)

conv1 = TimeDistributed(Conv2D(filters=filter_size, kernel_size=(3, 3), activation='relu'))(input_data)
conv1 = TimeDistributed(Dropout(0.8))(conv1)
conv1 = TimeDistributed(keras.layers.MaxPooling2D(pool_size=((1, 2))))(conv1)

conv2 = TimeDistributed(Conv2D(filters=filter_size, kernel_size=(1, 5), activation='relu'))(conv1)
conv2 = TimeDistributed(Dropout(0.8))(conv2)
conv3 = TimeDistributed(Conv2D(filters=filter_size, kernel_size=(1, 5), activation='relu'))(conv2)
maxpool = TimeDistributed(keras.layers.MaxPooling2D(pool_size=((1, 2))))(conv3)
maxpool = TimeDistributed(Dropout(0.8))(maxpool)

reshape = TimeDistributed(keras.layers.Reshape((1, -1)))(maxpool)  # (merge)
dense1 = TimeDistributed(Dense(units=fc_size, activation='relu', name="dense_1", kernel_initializer='glorot_uniform'))(
    reshape)
dense1 = TimeDistributed(Dropout(0.8))(dense1)

dense2 = TimeDistributed(Dense(units=fc_size, activation='relu', name="dense_2", kernel_initializer='glorot_uniform'))(
    dense1)
dense1 = TimeDistributed(Dropout(0.2))(dense1)
dense2 = TimeDistributed(Dense(units=fc_size, activation='relu', name="dense_2", kernel_initializer='glorot_uniform'))(
    dense1)

# Layer 5+6 Time Dist Dense Layer & Softmax
x = TimeDistributed(Dense(fc_size, activation='relu', kernel_initializer=init, bias_initializer=init))(dense2)
# x = TimeDistributed(Dropout(0.8))(x)
# y_pred = TimeDistributed(Dense(len(data_gen.y_encoding), name="y_pred", kernel_initializer=init,
#                                bias_initializer=init, activation="softmax"), name="out")(x)
output = TimeDistributed(Dense(len(data_gen.y_encoding), name="y_pred", kernel_initializer=init,
                               bias_initializer=init, activation="softmax"), name="out")(x)

labels = Input(name='the_labels', shape=[data_gen.phone_max_len], dtype='float32')
input_length = Input(name='input_length', shape=[1], dtype='int64')
label_length = Input(name='label_length', shape=[1], dtype='int64')

# Keras doesn't currently support loss funcs with extra parameters
# so CTC loss is implemented in a lambda layer
loss_out = Lambda(ctc_lambda_func, output_shape=(1,), name='ctc')([output, labels, input_length, label_length])


y_pred = Lambda(ctc_decode, output_shape=decode_output_shape, name='decoder')([output, input_length])
#              arguments={'is_greedy': True},

# out = Lambda(ctc_decode, output_shape=(300,), name='ctc_out')([y_pred, input_length])
adam = keras.optimizers.Adam(lr=0.000001)

# accuracy_ctc = Lambda(ctc_accuracy_func, )

# y_ctc_pred = K.ctc_label_dense_to_sparse(y_pred, data_gen.mel_max_num)

# model.compile(loss="categorical_crossentropy", optimizer=adam , metrics = [metrics.categorical_accuracy])

model = Model(inputs=[input_data, labels, input_length, label_length], outputs=[loss_out, y_pred, output])
model.compile(loss={'ctc': ctc_dummy_loss, 'decoder':decoder_dummy_loss},
              optimizer=adam, metrics={'decoder': ['accuracy']})
# model.compile(loss={'ctc': lambda y_true, y_pred: y_pred}, optimizer=adam, metrics={'dense_3': 'accuracy'})

#     reduce_lr=ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=3, verbose=1, mode='auto', epsilon=0.001, cooldown=0)
# stop  = EarlyStopping(monitor='val_loss', min_delta=0, patience=5, verbose=1, mode='auto')
tb_callback = keras.callbacks.TensorBoard(log_dir=TENSORBOARD_DIR, histogram_freq=0, write_graph=True)
print(model.summary())



hist_current = model.fit_generator(data_gen.train_generator(16),
                                   steps_per_epoch=500,
                                   epochs=6,
                                   # epochs=1000,
                                   verbose=1,
                                   validation_data=data_gen.test_generator(16),
#                                    validation_steps=1024,
                                   validation_steps=250,
                                   shuffle=False,
                                   callbacks=[tb_callback])

model.save('/media/yoonseok/Data/ModelBackup/2_model.h5')

import Utils.slack
Utils.slack.send_messeage('학습 끝. 빨리 확인하시요.')

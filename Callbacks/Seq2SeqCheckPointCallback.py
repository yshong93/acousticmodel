from keras.callbacks import Callback

class Seq2SeqCheckPointCallback(Callback):
    def __init__(self, model_dict):
        super().__init__()
        # if send_message:
            # self.send_messeage(send_message)
        self.model_dict = model_dict
        for path in model_dict.keys():
            model_dict[path].save(path)

    def on_epoch_end(self, epoch, logs={}):
        for path in self.model_dict.keys():
            self.model_dict[path].save(path)
        # if epoch % 10 == 0:
        # self.send_log_message(str(epoch) + ') \n' + str(logs))
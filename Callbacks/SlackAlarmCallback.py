from keras.callbacks import Callback

import slackweb


class SlackCallback(Callback):
    def __init__(self, send_message):
        super().__init__()
        if send_message:
            self.send_messeage(send_message)


    def on_epoch_end(self, epoch, logs={}):
        # if epoch % 10 == 0:
        self.send_log_message(str(epoch) + ') \n' + str(logs))



    def send_messeage(self, msg):
        slack_url = 'https://hooks.slack.com/services/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'
        slack = slackweb.Slack(url=slack_url)

        slack.notify(text=msg, channel="#notification", username="Keras")

    def send_log_message(self, msg):
        slack_url = 'https://hooks.slack.com/services/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'
        slack = slackweb.Slack(url=slack_url)

        slack.notify(text=msg, channel="#log", username="Keras")

# import keras.backend as K
from keras.callbacks import Callback
import tensorflow as tf

import Utils.slack
# from Utils.slack import send_messeage, send


class MetricsCallback(Callback):
    def __init__(self, mydir, test_data, y_encoding):
        super().__init__()
        self.writer = tf.summary.FileWriter(logdir=mydir)
        self.ler_log = []

        # self.ler_log.append(tf.Summary.Value(tag="LER", simple_value=10, epoch=epoch))
        # summary = tf.Summary(value=self.ler_log)
        # self.writer.add_summary(summary)

        self.y_encoding = y_encoding

        accuracy = None
        self.accuracy_summary = tf.Summary()
        self.accuracy_summary.value.add(tag='LER', simple_value=accuracy)

        self.test_input, _, self.test_output = test_data

    def on_epoch_end(self, epoch, logs={}):
        # logs.
        # score = self.eval_map()
        # print "MAP for epoch %d is %f"%(epoch, score)
        # self.maps.append(score)


        predict_data = self.model.predict(self.test_input)

        ler_list = []
        batch_size = len(predict_data[1])
        for num in range(batch_size):
            # y_pred print (CTC Decoded)
            predict_y = predict_data[1][num]
            predict_phones = [self.y_encoding[int(i)] for i in predict_y]
            # print(self.ler(self.test_output[num], predict_y))
            # print('1) %s \n2) %s' %(self.test_output[num], predict_phones))
            ler_list.append(self.ler(self.test_output[num], predict_phones))

        ler = sum(ler_list) / len(ler_list)
        # before training init writer (for tensorboard log) / model
        # writer = tf.summary.FileWriter()
        # model = ...

        # train model
        # loss = model.train_on_batch(...)
        # summary = tf.Summary(value=10)
        # summary = tf.Summary(value=tf.Summary.Value(tag='my_loss', simple_value=10))

        self.accuracy_summary.value[0].simple_value = ler
        self.writer.add_summary(self.accuracy_summary, epoch)

        if epoch % 5 == 0:
            predict_y = predict_data[1][0]
            predict_phones = [self.y_encoding[int(i)] for i in predict_y]
            Utils.slack.send_log_message('%d Epoch : %s' %(epoch, predict_phones))


    def ler(self, y_true, y_pred):
        return self.levenshteinDistance(y_pred, y_true) / len(y_true)

    def levenshteinDistance(self, s1, s2):
        if len(s1) > len(s2):
            s1, s2 = s2, s1

        distances = range(len(s1) + 1)
        for i2, c2 in enumerate(s2):
            distances_ = [i2 + 1]
            for i1, c1 in enumerate(s1):
                if c1 == c2:
                    distances_.append(distances[i1])
                else:
                    distances_.append(1 + min((distances[i1], distances[i1 + 1], distances_[-1])))
            distances = distances_
        return distances[-1]

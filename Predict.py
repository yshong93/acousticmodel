from Utils.script import Script
from DataGenerator import DataGenerator
from CTCUtils import ys_ctc_batch_cost, ctc_dummy_loss, decoder_dummy_loss
from Utils.Metrics import ler

import keras
from keras.activations import relu
from keras.models import load_model
import tensorflow as tf

import numpy as np

from KoG2P.YSg2p import sentence_to_KoG2P

def clipped_relu(x):
    return relu(x, max_value=20)

def predict(model_path, inputs):

    # wav_dir_path = '/media/yoonseok/Data/Corpus/Audio/korean_reading/feature4/'
    # model_path = '/media/yoonseok/Data/ModelBackup/3_CNN_model_short_.h5'

    # data_gen = DataGenerator(wav_dir_path)

    with tf.device('/cpu:0'):
        model = load_model(model_path,
                           {"ys_ctc_batch_cost" : ys_ctc_batch_cost,
                            "ctc_dummy_loss": ctc_dummy_loss,
                            'decoder_dummy_loss':decoder_dummy_loss,
                            'clipped_relu':clipped_relu})

        model.summary()
        # inputs, outputs = data_gen.test_generator(4)

        # print(inputs)
        predict_data = model.predict(inputs)
        print(predict_data)

        # CTC Temporal Label Print
        predict_ = np.squeeze(predict_data[2][1], axis=1)
        predict_.shape
        phones = np.argmax(predict_,axis=1)
        temp_labels = [data_gen.y_encoding[int(i)] for i in phones]
        print([label.replace("blank", "-") for label in temp_labels])

        # y_pred print
        predict_y = predict_data[1][1]
        print([data_gen.y_encoding[int(i)] for i in predict_y])
        # print([label.replace("blank", "-") for label in temp_labels])
        print('=====================================================')

def evaluate(model_path, inputs, trues):
    with tf.device('/cpu:0'):
        model = load_model(model_path,
                           {"ys_ctc_batch_cost" : ys_ctc_batch_cost,
                            "ctc_dummy_loss": ctc_dummy_loss,
                            'decoder_dummy_loss':decoder_dummy_loss,
                            'clipped_relu':clipped_relu})

        model.summary()

        predict_data = model.predict(inputs)


        batch_size = predict_data.shape[1]
        for num in range(batch_size):
            # y_pred print (CTC Decoded)
            predict_y = predict_data[1][num]
            predict_phones = [data_gen.y_encoding[int(i)] for i in predict_y]
            print(ler(trues[num], predict_phones))



import librosa
import os
import collections
import numpy as np

import math
from DataGenerator import DataGenerator

from Utils.phoneme import Phoneme


class PDataGenerator(DataGenerator):
    def __init__(self, wav_dir_path, max_sample_count = 160000):
        # super.__init__(wav_dir_path)
        self.wav_dir_path = wav_dir_path
        self.file_path_list = self.find_wav_files()

        self.phone_manager = Phoneme()
        self.y_encoding = self.phone_manager.get_phone_encoding()

        self.max_sample_count = max_sample_count

        self.mel_max_width = 2500
        self.mel_one_width = 8
        self.mel_max_num = math.ceil(self.mel_max_width / self.mel_one_width)
        self.mel_height = 40

        self.hop_length = 64

        self.phone_max_len = 300
        self.mel_shape = (None, self.mel_max_width, self.mel_height, 1)

    def find_wav_files(self, file_extension='.wav'):
        name_list = collections.deque()

        for (path, dir_, files) in os.walk(self.wav_dir_path):
            # sub_folder_name = self.wav_dir_path.split('/')[-1]
            #         print(sub_folder_name)
            for file_name in files:
                ext = os.path.splitext(file_name)[-1]
                if ext == file_extension:
                    # file = os.path.join(sub_folder_name, file_name)
                    file_full_path = os.path.join(path, file_name)
                    name_list.append(file_full_path)

        return name_list

    def get_batch_bylist(self, file_path):

        data_list = []
        for path in file_path:
            data_dict = {}
            raw, _ = librosa.load(path, sr=16000)
            data_dict['raw'] = raw
            # print(raw)
            data_dict['scrt'] = '더미데이터'

            data_list.append(data_dict)
        # data_list
        return self.get_batch(data_list, 2)


    def get_batch(self, data_list, size):
        size = len(data_list)

        # self.file_path_list

        # math.ceil(self.mel_max_width / self.mel_one_width)
        batch_max_raw_len = max([len(data['raw']) for data in data_list])
        mel_width_max_batch = math.ceil(int(batch_max_raw_len / self.hop_length) / self.mel_one_width)
        # phones_max_len_batch = int(max([for data in data_list]))

        X_data = np.zeros([size, mel_width_max_batch, self.mel_one_width, self.mel_height, 1])
        labels = np.zeros([size, self.phone_max_len])
        input_length = np.zeros([size, 1])
        label_length = np.zeros([size, 1])

        for idx, data in enumerate(data_list):
            raw = data['raw']
            # raw = librosa.util.normalize(raw)
            # raw = random_noise(raw)
            mel = self.feature_extract_x(raw)
            mel_time = mel.shape[0]
            #             print(mel.shape)
            #         mel = mel_list[idx]
            X_data[idx, :mel_time, :, :, :] = mel
            scrt = data['scrt']
            phones = self.sentence_to_phones(scrt)
            phones_list = self.feature_extract_y(phones=phones)
            phones_length = phones_list.shape[0]
            labels[idx, :phones_length] = phones_list

            input_length[idx] = mel_time
            label_length[idx] = phones_length

        inputs = {'the_input': X_data,
                  'the_labels': labels,
                  'input_length': input_length,
                  'label_length': label_length
                  }
        outputs = {'ctc': np.zeros([size]),
                   'decoder': np.zeros([size])}  # dummy data for dummy loss function
        # print(X_data.shape)
        return (inputs, outputs)

if __name__ == '__main__':
    # wav_dir_path = '~/WorkSpace/corpus/audio/korean_reading/fv01/'
    wav_dir_path = '/Users/YoonSeok/WorkSpace/corpus/audio'
    # wav_dir_path = '../../corpus/audio'
    # print(os.path.isdir(wav_dir_path))
    data_gen = PDataGenerator(wav_dir_path)
    # print(data_gen.find_wav_files())

    # inputs, _ = data_gen.get_batch_bylist(['/Users/YoonSeok/WorkSpace/corpus/audio/korean_reading/mw08_t03_s10_m.wav',
    #                                   '/Users/YoonSeok/WorkSpace/corpus/audio/korean_reading/mw08_t03_s10_m.wav'])

    inputs, _ = data_gen.get_batch_bylist(['/Users/YoonSeok/WorkSpace/corpus/audio/korean_reading/fv01/fv01_t01_s17.wav',
                                           '/Users/YoonSeok/WorkSpace/corpus/audio/korean_reading/fv01/fv01_t01_s17.wav'])

    model_path = '/Users/YoonSeok/WorkSpace/ModelBackup/DeepCNN_LSTM_1.h5'
    predict(model_path=model_path, inputs=inputs)

    print('11111111111111')

    # sentence = Script().get_script_from_num(3, 10)
    sentence = Script().get_script_from_num(1, 17)
    phonemes_kog2p = sentence_to_KoG2P(sentence)
    phone_list = phonemes_kog2p.split(' ')
    print(Phoneme().transcript_Kog2p(phone_list))
    phone_manager = Phoneme()

    print()
    # print(Script().get_script_from_num(3, 10))

    # raw ,sr = librosa.load(data_gen.file_path_list.pop(), sr=16000)
    # print(len(raw))
    # with tf.device('/cpu:0'):
    #     model = load_model(model_path,
    #                        {"ys_ctc_batch_cost" : ys_ctc_batch_cost,
    #                         "ctc_dummy_loss": ctc_dummy_loss,
    #                         'decoder_dummy_loss':decoder_dummy_loss})
    #     model.summary()
    #     predict_data = model.predict(inputs)
    #     print(predict_data)
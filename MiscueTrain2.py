from CTCUtils import ys_ctc_batch_cost, ctc_dummy_loss, decoder_dummy_loss
from MiscueDataGenerator import MiscueDataGenerator

import keras
from keras.models import Model
from keras.utils import multi_gpu_model
from keras.activations import relu
from keras.models import load_model

import numpy as np


class ModelMGPU(Model):
    def __init__(self, ser_model, gpus):
        pmodel = multi_gpu_model(ser_model, gpus)
        self.__dict__.update(pmodel.__dict__)
        self._smodel = ser_model

    def __getattribute__(self, attrname):
        '''Override load and save methods to be used from the serial-model. The
        serial-model holds references to the weights in the multi-gpu model.
        '''
        # return Model.__getattribute__(self, attrname)
        if 'load' in attrname or 'save' in attrname:
            return getattr(self._smodel, attrname)

        return super(ModelMGPU, self).__getattribute__(attrname)

def clipped_relu(x):
    return relu(x, max_value=20)

wav_dir_path = '/media/yoonseok/Data/Corpus/Audio/korean_reading/feature5/'
model_path = '/media/yoonseok/Data/ModelBackup/DeepCNN/DeepCNN_model_4.h5'

TENSORBOARD_DIR = '/media/yoonseok/Data/TensorBoard/ReciteMiscue/biLSTM-2'




data_generator = MiscueDataGenerator(wav_dir_path=wav_dir_path)

import keras
from keras.layers import Input, Dense, TimeDistributed, LSTM, Bidirectional
from keras import Model
import tensorflow as tf
from keras.callbacks import ModelCheckpoint
# from keras.activations import Adam
# from keras.model import Model
# model = Model(inputs=[input_data, labels, input_length, label_length], outputs=[loss_out, y_pred, output])


# input_data = model.inputs[0]



with tf.device('/cpu:0'):
    model = load_model(model_path,
                       {"ys_ctc_batch_cost": ys_ctc_batch_cost,
                        "ctc_dummy_loss": ctc_dummy_loss,
                        'decoder_dummy_loss': decoder_dummy_loss,
                        'clipped_relu': clipped_relu})

    for layer in model.layers:
        layer.trainable = False
    softmax = model.outputs[2]

    # keras.layers.SimpleRNN(units, activation='tanh', use_bias=True, kernel_initializer='glorot_uniform', recurrent_initializer='orthogonal', bias_initializer='zeros', kernel_regularizer=None, recurrent_regularizer=None, bias_regularizer=None, activity_regularizer=None, kernel_constraint=None, recurrent_constraint=None, bias_constraint=None, dropout=0.0, recurrent_dropout=0.0, return_sequences=False, return_state=False, go_backwards=False, stateful=False, unroll=False)
    x = TimeDistributed(keras.layers.Reshape((-1,)), name='miscue_TD_0')(softmax)
    # x = TimeDistributed(Dense(units=1024, activation='relu', name='miscue_dens_1'), name='miscue_TD_1')(x)

    # x = TimeDistributed(Dense(units=1024, activation='relu', kernel_initializer='glorot_uniform', name='miscue_dens_2'), name='miscue_TD_2')(x)

    x = Bidirectional(LSTM(256, return_sequences=True, activation='relu', dropout=0.8,
                           kernel_initializer='he_normal', name='miscue_LSTM'), merge_mode='sum')(x)

    # x = TimeDistributed(Dense(units=1024, activation='relu', name="dense_3", kernel_initializer='glorot_uniform'))(x)
    x = TimeDistributed(Dense(units=7, activation="softmax", kernel_initializer='glorot_uniform'), name="miscue")(x)

    adam = keras.optimizers.Adam(lr=0.00005)

    miscueModel = Model(inputs=model.inputs, outputs=[x])

p_miscueModel = ModelMGPU(miscueModel , 2)

p_miscueModel.compile(loss='categorical_crossentropy', optimizer=adam, metrics=[keras.metrics.categorical_accuracy])


# for layer in miscueModel.layers[:-4]:
#     layer.trainable = False


# miscueModel.metrics_tensors = [x]


tb_callback = keras.callbacks.TensorBoard(log_dir=TENSORBOARD_DIR, histogram_freq=0, write_graph=True)

model_save_path = '/media/yoonseok/Data/ModelBackup/Miscue/Miscue_biLSTM_2.h5'
checkpoint = ModelCheckpoint(model_save_path, monitor='categorical_accuracy', verbose=1)

p_miscueModel.summary()
hist_current = p_miscueModel.fit_generator(data_generator.train_generator(24),
                                        steps_per_epoch=100,
                                        epochs=300,
                                        callbacks=[tb_callback, checkpoint],
                                        initial_epoch=0)




# hist_current = parallel_model.fit_generator(data_gen.train_generator(24),
#                                    steps_per_epoch=500,
#                                    epochs=60,
#                                    # epochs=1000,
#                                    verbose=1,
#                                    validation_data=data_gen.test_generator(24),
# #                                    validation_steps=1024,
#                                    validation_steps=324, # 7767
#                                    shuffle=False,
#                                    callbacks=[tb_callback, checkpoint, reduce_lr],
#                                    initial_epoch=0)
import Utils.slack
Utils.slack.send_messeage('학습 끝. 빨리 확인하시요.')

p_miscueModel.save(model_save_path)

Utils.slack.send_messeage('모델 저장 완료 : %s' %(model_save_path))
# for i in range(1000):
#     predict_data, miscue = miscueTrain.predict()
#     hist_current = miscueModel.fit(x = predict_data, y = miscue['miscue'])
from Utils.MiscueMaker import NoiseMaker

import librosa

import pprint

test_path = '/media/yoonseok/Data/Corpus/Audio/korean_reading/audio/fv01/fv01_t01_s02.wav'
test_label_path = '/media/yoonseok/Data/Corpus/Audio/korean_reading/label/fv01_t01_s01.textgrid'

raw, sr = librosa.load(test_path, sr=16000)
for i in range(100):
    nm = NoiseMaker(test_path=test_path, test_label_path=test_label_path)
    a, b = nm.random_PAU(1.1)
    print(b)

label_width = 8*64
pprint.pprint(nm.tgm.textgrid_to_ctc(test_label_path, int(len(raw)/label_width)))
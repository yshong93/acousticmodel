from DataGenerator import DataGenerator
from Utils.MiscueMaker import NoiseMaker, MiscueMaker
from AudioUtils import random_noise

import logging
logging.basicConfig(filename='./log/miscue2.log', level=logging.DEBUG)

import math
import librosa

import numpy as np
# import Utils.
import os
import random

class MiscueDataGenerator(DataGenerator):
    def __init__(self, wav_dir_path, max_sample_count=160000):
        super(MiscueDataGenerator, self).__init__(wav_dir_path=wav_dir_path, max_sample_count=max_sample_count)
        self.miscueMaker = MiscueMaker(self.hop_length, self.mel_one_width)

    def get_textgrid_path(self, file_path):
        text_grid_file_path = file_path.split('/')[1].split('.')[0] + '.textgrid'
        return '/media/yoonseok/Data/Corpus/Audio/korean_reading/label/' + text_grid_file_path

    def one_hot_encoding(self, data_list, nb_classes):
        one_hot_targets = np.zeros((len(data_list), nb_classes))
        targets = np.array([data_list], dtype=np.int8).reshape(-1)
        # one_hot_targets = np.eye(nb_classes)[targets]
        for idx, data in enumerate(data_list):
            one_hot_targets[idx][int(data)] = 1
        return one_hot_targets

    # def new_get_batch(self, data_list, size):
    #     inputs = {'the_input': [],
    #               'the_input': []}

    def get_batch(self, data_list, size):
        size = len(data_list)

        # batch_max_raw_len = max([len(data['raw']) for data in data_list])
        batch_max_raw_len = 200000
        mel_width_max_batch = math.ceil(int(batch_max_raw_len / self.hop_length) / self.mel_one_width)
        # mel_width_max_batch = 350
        # mel_widt_max_batch += 1


        X_data = np.zeros([size, mel_width_max_batch, self.mel_one_width, self.mel_height, 1])
        labels = np.zeros([size, self.phone_max_len])
        input_length = np.zeros([size, 1])
        label_length = np.zeros([size, 1])
        the_miscue = np.zeros([size, mel_width_max_batch, 7])
        miscue_seq2seq_target = np.zeros([size, mel_width_max_batch, 7])

        for idx, data in enumerate(data_list):
            raw = data['raw']
            raw = librosa.util.normalize(raw)
            raw = random_noise(raw)
            mel = self.feature_extract_x(raw)
            mel_time = mel.shape[0]

            X_data[idx, :mel_time, :, :, :] = mel

            scrt = data['scrt']
            phones = self.sentence_to_phones(scrt)
            phones_list = self.feature_extract_y(phones=phones)
            phones_length = phones_list.shape[0]
            labels[idx, :phones_length] = phones_list

            input_length[idx] = mel_time
            label_length[idx] = phones_length

            miscue_ctc = self.miscueMaker.miscue_interval_to_ctc(data['raw'], data['miscue'], mel_time, self.miscueMaker.tgm.miscue.encoding)
            miscue_annot = self.one_hot_encoding(miscue_ctc, 7)
            the_miscue[idx, 1:miscue_annot.shape[0]+1, :] = miscue_annot
            the_miscue[idx, miscue_annot.shape[0] + 1, 6] = 1
            the_miscue[idx, 1, 1] = 1

            miscue_seq2seq_target[idx, :miscue_annot.shape[0], :] = miscue_annot
            miscue_seq2seq_target[idx, miscue_annot.shape[0], 6] = 1
            # miscue_seq2seq_target[idx, 0, :] = np.zeros([7])

            # print('%d / %d' %(mel_time, miscue_annot.shape[0]))
        inputs = {'the_input': X_data,
                  'the_labels': labels,
                  'input_length': input_length,
                  'label_length': label_length,
                  'the_miscue': the_miscue,
                  }
        outputs = {
                    'miscue': miscue_seq2seq_target,
                    'ctc': np.zeros([size]),
                    'decoder': np.zeros([size])
                   }
        return (inputs, outputs)

    def make_miscue(self, data):
        raw = data['raw']
        path = data['path']
        scrt = data['scrt']

        textgrid_path = self.get_textgrid_path(path)

        miscue_interval_list = []
        try:
            random_func = random.choice([self.miscueMaker.random_PAU, self.miscueMaker.random_EXT, self.miscueMaker.random_PRE])
            miscue_raw, miscue_interval_list = random_func(raw, textgrid_path, miscue_interval_list)
            # miscue_raw, miscue_interval_list = random_func(miscue_raw, textgrid_path, miscue_interval_list)
        except:
            miscue_raw, miscue_interval_list = self.miscueMaker.null_miscue(raw, textgrid_path, miscue_interval_list)
            logging.debug(data['path'])


        miscue_data = {}
        miscue_data['raw'] = miscue_raw
        miscue_data['miscue'] = miscue_interval_list

        miscue_data['scrt'] = scrt
        miscue_data['path'] = path
        return miscue_data

    def train_generator(self, batchSize):
        batch_data_list = []
        while True:
            for numpy_name in self.trainData:
                data_list = np.load(self.wav_dir_path + numpy_name)

                for data in data_list:
                    if len(data['raw']) <= self.max_sample_count and len(data['raw']) >= 16000 and os.path.exists(self.get_textgrid_path(data['path'])):
                        miscue_data = self.make_miscue(data)
                        if len(miscue_data['raw']) <= self.max_sample_count and len(miscue_data['raw']) >= 16000:
                            batch_data_list.append(miscue_data)
                    if len(batch_data_list) == batchSize:
                        #                         print('train_gen')
                        # try:
                        yield self.get_batch(batch_data_list, batchSize)
                        batch_data_list = []
                        # except:
                        #     print('error : %s' % [data['path'] for data in batch_data_list])
                        #     batch_data_list = []
                        #     continue

    def valid_generator(self, batchSize):
        batch_data_list = []
        for numpy_name in self.testData:
            # for numpy_name in self.testData:
            data_list = np.load(self.wav_dir_path + numpy_name)

            while True:
                for data in data_list:
                    if len(data['raw']) <= self.max_sample_count and len(data['raw']) >= 16000 and os.path.exists(self.get_textgrid_path(data['path'])):
                        miscue_data = self.make_miscue(data)
                        miscue_data['miscue'] = []
                        batch_data_list.append(miscue_data)
                    if len(batch_data_list) == batchSize:
                        yield self.get_batch(batch_data_list, batchSize)
                        batch_data_list = []

    def test_generator(self, miscue_dir_path, batchSize):
        audio_path_list = []
        for (path, dirs, files) in os.walk(miscue_dir_path):
            for file in files:
                if '.wav' in file:
                    audio_path_list.append(os.path.join(path, file))

        batch_data_list = []
        for audio_path in audio_path_list:
            data = {}
            data['raw'], _ = librosa.load(audio_path, sr=16000)
            data['scrt'] = '안녕하세요'
            data['path'] = audio_path
            data['miscue'] = []
            batch_data_list.append(data)
            if len(batch_data_list) >= batchSize:
                yield self.get_batch(batch_data_list, batchSize)
                batch_data_list=[]
# import os
# os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"   # see issue #152
# os.environ["CUDA_VISIBLE_DEVICES"] = ""

from CTCUtils import ys_ctc_batch_cost, ctc_dummy_loss, decoder_dummy_loss
from NewMiscueGenerator import MiscueDataGenerator

import keras
from keras.models import Model
from keras.utils import multi_gpu_model
from keras.activations import relu
from keras.models import load_model

import numpy as np


class ModelMGPU(Model):
    def __init__(self, ser_model, gpus):
        pmodel = multi_gpu_model(ser_model, gpus)
        self.__dict__.update(pmodel.__dict__)
        self._smodel = ser_model

    def __getattribute__(self, attrname):
        '''Override load and save methods to be used from the serial-model. The
        serial-model holds references to the weights in the multi-gpu model.
        '''
        # return Model.__getattribute__(self, attrname)
        if 'load' in attrname or 'save' in attrname:
            return getattr(self._smodel, attrname)

        return super(ModelMGPU, self).__getattribute__(attrname)


def clipped_relu(x):
    return relu(x, max_value=20)


wav_dir_path = '/media/yoonseok/Data/Corpus/Audio/korean_reading/feature5/'
model_path = '/media/yoonseok/Data/ModelBackup/DeepCNN/DeepCNN_model_4.h5'

MODEL_NAME = 'Seq2Seq_BiGRU'
TENSORBOARD_DIR = '/media/yoonseok/Data/TensorBoard/ReciteMiscue/' + MODEL_NAME


data_generator = MiscueDataGenerator(wav_dir_path=wav_dir_path)

import keras
from keras.layers import Input, Dense, TimeDistributed, LSTM, Bidirectional, GRU, RepeatVector, Dropout, merge
from keras import Model
import tensorflow as tf
from keras.callbacks import ModelCheckpoint

from Callbacks.SlackAlarmCallback import SlackCallback

# input_data = model.inputs[0]
with tf.device('/gpu:0'):
    model = load_model(model_path,
                           {"ys_ctc_batch_cost": ys_ctc_batch_cost,
                            "ctc_dummy_loss": ctc_dummy_loss,
                            'decoder_dummy_loss': decoder_dummy_loss,
                            'clipped_relu': clipped_relu})

    for layer in model.layers:
        layer.trainable = False

# with tf.device('/cpu:0'):
with tf.device('/gpu:1'):

    softmax = model.outputs[2]

    x = TimeDistributed(keras.layers.Reshape((-1,)), name='miscue_TD_0')(softmax)

    latent_dim = 256
    #     encoder_inputs = Input(shape=(None, num_encoder_tokens))
    #encoder_gru = GRU(latent_dim, return_state=True, return_sequences=True, dropout=0.2, recurrent_dropout=0.2)
    #encoder = Bidirectional(encoder_gru, merge_mode='sum')

    encoder_gru_left = GRU(latent_dim, return_state=True, dropout=0.2, recurrent_dropout=0.2)
    encoder_outputs_l, state_h_l = encoder_gru_left(x)

    encoder_gru_right = GRU(latent_dim, return_state=True, dropout=0.2, recurrent_dropout=0.2, go_backwards=True)
    encoder_outputs_r, state_h_r = encoder_gru_right(x)
    
    encoder_outputs = merge([encoder_outputs_l, encoder_outputs_r], mode='sum')
    state_h = merge([state_h_l, state_h_r], mode='sum')
    
    #encoder_outputs, state_h = encoder(x)
    encoder_states = [state_h]

    # Set up the decoder, using `encoder_states` as initial state.
    decoder_inputs = Input(shape=(None, len(data_generator.miscue_encoding)), name='the_miscue')

    decoder_lstm = GRU(latent_dim, return_sequences=True, return_state=True, dropout=0.2, recurrent_dropout=0.2)
    deocder_lstm = Bidirectional(decoder_lstm, merge_mode='sum')

    decoder_outputs, _ = decoder_lstm(decoder_inputs,
                                         initial_state=encoder_states)
    decoder_dense = Dense(len(data_generator.miscue_encoding), activation='softmax', name='miscue')
    decoder_outputs = decoder_dense(decoder_outputs)

    # Define the model that will turn
    # `encoder_input_data` & `decoder_input_data` into `decoder_target_data`
    #     model = Model([encoder_inputs, decoder_inputs], decoder_outputs)


    adam = keras.optimizers.Adam(lr=0.01)
    # keras.losses.q

    input_layers = [input for input in model.inputs]
    print(model.inputs[0])
    print(model.inputs)
    import keras.backend as K

    K.set_learning_phase(1)
    miscueModel = Model(inputs=[model.inputs[0], model.inputs[1], model.inputs[2], decoder_inputs],
                        outputs=[decoder_outputs])
    # miscueModel = Model(inputs=[model.inputs[0], model.inputs[1], model.inputs[2]], outputs=[outputs])

# p_miscueModel = ModelMGPU(miscueModel , 2)
p_miscueModel = miscueModel
p_miscueModel.compile(loss='categorical_crossentropy', optimizer=adam, metrics=[keras.metrics.categorical_accuracy])

encoder_model = Model([model.inputs[0], model.inputs[1], model.inputs[2]], encoder_states)

decoder_state_input_h = Input(shape=(latent_dim,))
# decoder_state_input_c = Input(shape=(latent_dim,))
decoder_states_inputs = [decoder_state_input_h]
decoder_outputs, state_h = decoder_lstm(
    decoder_inputs, initial_state=decoder_states_inputs)
decoder_states = [state_h]
decoder_outputs = decoder_dense(decoder_outputs)
decoder_model = Model(
    [decoder_inputs] + decoder_states_inputs,
    [decoder_outputs] + decoder_states)


model_save_path = '/media/yoonseok/Data/ModelBackup/Miscue/' + MODEL_NAME + '.h5'
p_miscueModel.save(model_save_path)

encoder_model_save_path = '/media/yoonseok/Data/ModelBackup/Miscue/' + MODEL_NAME + '_encoder.h5'
encoder_model.save(encoder_model_save_path)

decoder_model_save_path = '/media/yoonseok/Data/ModelBackup/Miscue/' + MODEL_NAME + '_deocder.h5'
decoder_model.save(decoder_model_save_path)


# miscueModel.metrics_tensors = [x]


tb_callback = keras.callbacks.TensorBoard(log_dir=TENSORBOARD_DIR, histogram_freq=0, write_graph=True)

reduce_lr = keras.callbacks.ReduceLROnPlateau(monitor='categorical_accuracy', factor=0.5, patience=3, verbose=1,
                                              mode='auto',
                                              epsilon=0.001, cooldown=0)

auto_model_save_path = '/media/yoonseok/Data/ModelBackup/Miscue/' + MODEL_NAME + '_auto.h5'
checkpoint = ModelCheckpoint(auto_model_save_path, monitor='categorical_accuracy', verbose=1)

from Callbacks.Seq2SeqCheckPointCallback import Seq2SeqCheckPointCallback
checkpoint_callback = Seq2SeqCheckPointCallback({model_save_path:p_miscueModel,
                                                 encoder_model_save_path:encoder_model,
                                                 decoder_model_save_path:decoder_model})
p_miscueModel.summary()
hist_current = p_miscueModel.fit_generator(data_generator.train_generator(32),
                                           steps_per_epoch=100,
                                           epochs=50,
                                           callbacks=[tb_callback, checkpoint, reduce_lr, SlackCallback(MODEL_NAME)],
                                           initial_epoch=0)

# hist_current = parallel_model.fit_generator(data_gen.train_generator(24),
#                                    steps_per_epoch=500,
#                                    epochs=60,
#                                    # epochs=1000,
#                                    verbose=1,
#                                    validation_data=data_gen.test_generator(24),
# #                                    validation_steps=1024,
#                                    validation_steps=324, # 7767
#                                    shuffle=False,
#                                    callbacks=[tb_callback, checkpoint, reduce_lr],
#                                    initial_epoch=0)

decoder_state_input_h = Input(shape=(latent_dim,))
# decoder_state_input_c = Input(shape=(latent_dim,))
decoder_states_inputs = [decoder_state_input_h]
decoder_outputs, state_h = decoder_lstm(
    decoder_inputs, initial_state=decoder_states_inputs)
decoder_states = [state_h]
decoder_outputs = decoder_dense(decoder_outputs)
decoder_model = Model(
    [decoder_inputs] + decoder_states_inputs,
    [decoder_outputs] + decoder_states)

import Utils.slack

Utils.slack.send_messeage('학습 끝. 빨리 확인하시요.')

model_save_path = '/media/yoonseok/Data/ModelBackup/Miscue/' + MODEL_NAME + '.h5'
p_miscueModel.save(model_save_path)

encoder_model_save_path = '/media/yoonseok/Data/ModelBackup/Miscue/' + MODEL_NAME + '_encoder.h5'
encoder_model.save(encoder_model_save_path)

decoder_model_save_path = '/media/yoonseok/Data/ModelBackup/Miscue/' + MODEL_NAME + '_deocder.h5'
decoder_model.save(decoder_model_save_path)

Utils.slack.send_messeage('모델 저장 완료 : %s' % (model_save_path))
# for i in range(1000):
#     predict_data, miscue = miscueTrain.predict()
#     hist_current = miscueModel.fit(x = predict_data, y = miscue['miscue'])
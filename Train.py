import os
# os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
# os.environ["CUDA_VISIBLE_DEVICES"]="1"

TENSORBOARD_DIR = '/media/yoonseok/Data/TensorBoard/'+\
                  'ReciteCTC-fix-epoch/DeepCNN-1'
wav_dir_path = '/media/yoonseok/Data/Corpus/Audio/korean_reading/feature4/'

from DataGenerator import DataGenerator
from CTCUtils import *
from Utils.phoneme import Phoneme

from Model.Metric import batch_WER, wer
from keras.callbacks import ModelCheckpoint, ReduceLROnPlateau
from keras.models import load_model

import keras

data_gen = DataGenerator(wav_dir_path)
# print(Phoneme().get_phone_encoding())
data_gen.y_encoding = Phoneme().get_phone_encoding()
filter_size = 512

model = load_model('/media/yoonseok/Data/ModelBackup/DeepCNN/1_CNN_model.h5', 
                   {"ys_ctc_batch_cost":ys_ctc_batch_cost, "ctc_dummy_loss": ctc_dummy_loss, 'decoder_dummy_loss':decoder_dummy_loss})
print(model.summary())


checkpoint = ModelCheckpoint('/media/yoonseok/Data/ModelBackup/DeepCNN/DeepCNN_model.h5', monitor='val_ctc_loss', verbose=1, save_best_only=True, mode='max')
reduce_lr= ReduceLROnPlateau(monitor='val_ctc_loss', factor=0.1, patience=3, verbose=1, mode='auto', epsilon=0.001, cooldown=0)
# stop  = EarlyStopping(monitor='val_loss', min_delta=0, patience=5, verbose=1, mode='auto')
tb_callback = keras.callbacks.TensorBoard(log_dir=TENSORBOARD_DIR, histogram_freq=0, write_graph=True)
print(model.summary())




hist_current = model.fit_generator(data_gen.train_generator(16),
                                   steps_per_epoch=500,
                                   epochs=100,
                                   # epochs=1000,
                                   verbose=1,
                                   validation_data=data_gen.test_generator(16),
#                                    validation_steps=1024,
                                   validation_steps=250,
                                   shuffle=False,
                                   callbacks=[tb_callback, checkpoint, reduce_lr],
                                   initial_epoch=20)

model.save('/media/yoonseok/Data/ModelBackup/DeepCNN/2_CNN_model.h5')

import Utils.slack
Utils.slack.send_messeage('학습 끝. 빨리 확인하시요.')
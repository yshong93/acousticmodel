from CTCUtils import ys_ctc_batch_cost, ctc_dummy_loss, decoder_dummy_loss
from MiscueDataGenerator import MiscueDataGenerator

import keras
from keras.activations import relu
from keras.models import load_model
import tensorflow as tf

import numpy as np

from KoG2P.YSg2p import sentence_to_KoG2P


def clipped_relu(x):
    return relu(x, max_value=20)


class MiscueTrain(object):
    def __init__(self, model_path, wav_dir_path):
        self.model = load_model(model_path,
                                {"ys_ctc_batch_cost": ys_ctc_batch_cost,
                                 "ctc_dummy_loss": ctc_dummy_loss,
                                 'decoder_dummy_loss': decoder_dummy_loss,
                                 'clipped_relu': clipped_relu})

        #         self.model.summary()
        self.wav_dir_path = wav_dir_path

    #         self.data_gen = MiscueDataGenerator(wav_dir_path)
    #         self.train_generator = self.data_gen.train_generator(24)

    def predict(self):
        #         with tf.device('/cpu:0'):
        generator = self.generate_train_data(20)
        steps = 1000
        model = self.model
        #         ler_list = []
        #         for step in range(steps):
        inputs, outputs, miscue = next(generator)
        predict_data = model.predict(inputs)

        batch_size = len(predict_data[2])
        for num in range(batch_size):
            # y_pred print (CTC Decoded)
            predict_y = predict_data[2][num]
        # predict_phones = [data_gen.y_encoding[int(i)] for i in predict_y]
        #                 print(predict_y)

        #         print('####### %d Now!!!' % step)
        return predict_data[2], miscue

    def generate_train_data(self, batch_size):
        data_gen = MiscueDataGenerator(self.wav_dir_path)
        train_data_gen = data_gen.train_generator(24)
        model = self.model

        while True:
            inputs, outputs, miscue = next(train_data_gen)
            predict_data = model.predict(inputs)
            print(predict_data)
            yield (predict_data[2], miscue['miscue'])

model_path = '/media/yoonseok/Data/ModelBackup/DeepCNN/DeepCNN_model_4.h5'
wav_dir_path = '/media/yoonseok/Data/Corpus/Audio/korean_reading/feature5/'

TENSORBOARD_DIR = '/media/yoonseok/Data/TensorBoard/ReciteMiscue/NN-1'



import keras
from keras.layers import Input, Dense, TimeDistributed
from keras import Model
# from keras.activations import Adam
# from keras.model import Model

input_data = Input(shape=(None, 1, 50), name='the_input')
# keras.layers.SimpleRNN(units, activation='tanh', use_bias=True, kernel_initializer='glorot_uniform', recurrent_initializer='orthogonal', bias_initializer='zeros', kernel_regularizer=None, recurrent_regularizer=None, bias_regularizer=None, activity_regularizer=None, kernel_constraint=None, recurrent_constraint=None, bias_constraint=None, dropout=0.0, recurrent_dropout=0.0, return_sequences=False, return_state=False, go_backwards=False, stateful=False, unroll=False)
x = TimeDistributed(keras.layers.Reshape((-1,)))(input_data)
x = TimeDistributed(Dense(units=1024, activation='relu'))(x)
x = TimeDistributed(Dense(units=1024, activation='relu', name="dense_2", kernel_initializer='glorot_uniform'))(x)

x = TimeDistributed(Dense(units=1024, activation='relu', name="dense_3", kernel_initializer='glorot_uniform'))(x)
x = TimeDistributed(Dense(units=7, activation="softmax", name="dense_4", kernel_initializer='glorot_uniform'))(x)

adam = keras.optimizers.Adam(lr=0.0001)

miscueModel = Model(inputs=[input_data], outputs=[x])
miscueModel.compile(loss='categorical_crossentropy', optimizer=adam)


# tb_callback = keras.callbacks.TensorBoard(log_dir=TENSORBOARD_DIR, histogram_freq=0, write_graph=True)
#
miscueTrain = MiscueTrain(model_path, wav_dir_path)
#
# hist_current = miscueModel.fit_generator(miscueTrain.generate_train_data(24),
#                                         steps_per_epoch=500,
#                                         epochs=60,
#                                         callbacks=[tb_callback],
#                                         initial_epoch=0)

# hist_current = parallel_model.fit_generator(data_gen.train_generator(24),
#                                    steps_per_epoch=500,
#                                    epochs=60,
#                                    # epochs=1000,
#                                    verbose=1,
#                                    validation_data=data_gen.test_generator(24),
# #                                    validation_steps=1024,
#                                    validation_steps=324, # 7767
#                                    shuffle=False,
#                                    callbacks=[tb_callback, checkpoint, reduce_lr],
#                                    initial_epoch=0)

for i in range(1000):
    predict_data, miscue = miscueTrain.predict()
    hist_current = miscueModel.fit(x = predict_data, y = miscue['miscue'])
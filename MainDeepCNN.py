import os
# os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
# os.environ["CUDA_VISIBLE_DEVICES"]="1"
from config import phoneme_classifier_config

config = phoneme_classifier_config
TENSORBOARD_DIR = config['TENSORBOARD_DIR']
wav_dir_path = config['RAW_FEATURE_DIR']
model_backup_path = config['MODEL_PATH']

# from DataReadGenerator import DataGenerator
from DataGenerator import DataGenerator
from CTCUtils import *
from Utils.phoneme import Phoneme
from Utils.ModelGPU import ModelMGPU

from keras.callbacks import ModelCheckpoint, ReduceLROnPlateau

import keras
from keras.layers import Input, Conv2D, Dense, Dropout, TimeDistributed, Lambda

from keras.models import Model
from keras.initializers import random_normal

data_gen = DataGenerator(wav_dir_path)
# print(Phoneme().get_phone_encoding())
data_gen.y_encoding = Phoneme().get_phone_encoding()
filter_size = 512

with tf.device('/cpu:0'):
    input_data = Input(shape=(None, data_gen.mel_one_width, data_gen.mel_height, 1), name='the_input')

    fc_size = 2048
    # First 3 FC layers
    init = random_normal(stddev=0.046875)

    conv1 = TimeDistributed(Conv2D(filters=filter_size, kernel_size=(8, 5), activation='relu'))(input_data)
    conv1 = TimeDistributed(Dropout(0.5))(conv1)
    conv1 = TimeDistributed(keras.layers.MaxPooling2D(pool_size=((1, 2))))(conv1)

    conv2 = TimeDistributed(Conv2D(filters=filter_size, kernel_size=(1, 5), activation='relu'))(conv1)
    conv2 = TimeDistributed(Dropout(0.5))(conv2)
    conv2 = TimeDistributed(keras.layers.MaxPooling2D(pool_size=((1, 2))))(conv2)

    conv3 = TimeDistributed(Conv2D(filters=filter_size, kernel_size=(1, 5), activation='relu'))(conv2)
    conv3 = TimeDistributed(Dropout(0.5))(conv3)

    reshape = TimeDistributed(keras.layers.Reshape((1, -1)))(conv3)  # (merge)
    dense1 = TimeDistributed(Dense(units=fc_size, activation='relu', name="dense_1", kernel_initializer='glorot_uniform'))(
        reshape)
    dense1 = TimeDistributed(Dropout(0.5))(dense1)

    dense2 = TimeDistributed(Dense(units=fc_size, activation='relu', name="dense_2", kernel_initializer='glorot_uniform'))(
        dense1)
    dense2 = TimeDistributed(Dropout(0.5))(dense2)

    x = TimeDistributed(Dense(fc_size, activation='relu', kernel_initializer=init, bias_initializer=init))(dense2)
    x = TimeDistributed(Dropout(0.5))(x)

    # x = TimeDistributed(Dropout(0.8))(x)
    # y_pred = TimeDistributed(Dense(len(data_gen.y_encoding), name="y_pred", kernel_initializer=init,
    #                                bias_initializer=init, activation="softmax"), name="out")(x)
    output = TimeDistributed(Dense(len(data_gen.y_encoding), name="y_pred", kernel_initializer=init,
                                   bias_initializer=init, activation="softmax"), name="out")(x)

    labels = Input(name='the_labels', shape=[data_gen.phone_max_len], dtype='float32')
    input_length = Input(name='input_length', shape=[1], dtype='int64')
    label_length = Input(name='label_length', shape=[1], dtype='int64')

    # Keras doesn't currently support loss funcs with extra parameters
    # so CTC loss is implemented in a lambda layer
    loss_out = Lambda(ctc_lambda_func, output_shape=(1,), name='ctc')([output, labels, input_length, label_length])


    y_pred = Lambda(ctc_decode, output_shape=decode_output_shape, name='decoder')([output, labels, input_length, label_length])
    #              arguments={'is_greedy': True},

    # out = Lambda(ctc_decode, output_shape=(300,), name='ctc_out')([y_pred, input_length])
    adam = keras.optimizers.Adam(lr=0.0005)

    # accuracy_ctc = Lambda(ctc_accuracy_func, )

    # y_ctc_pred = K.ctc_label_dense_to_sparse(y_pred, data_gen.mel_max_num)

    # model.compile(loss="categorical_crossentropy", optimizer=adam , metrics = [metrics.categorical_accuracy])

    model = Model(inputs=[input_data, labels, input_length, label_length], outputs=[loss_out, y_pred, output])

parallel_model = ModelMGPU(model , 2)


parallel_model.compile(loss={'ctc': ctc_dummy_loss, 'decoder':decoder_dummy_loss}, optimizer=adam)
# model.compile(loss={'ctc': lambda y_true, y_pred: y_pred}, optimizer=adam, metrics={'dense_3': 'accuracy'})

checkpoint = ModelCheckpoint(model_backup_path,
                             monitor='ctc_loss', verbose=1, mode='max')
reduce_lr= ReduceLROnPlateau(monitor='ctc_loss', factor=0.1, patience=10, verbose=1, mode='auto',
                             epsilon=0.001, cooldown=0)
tb_callback = keras.callbacks.TensorBoard(log_dir=TENSORBOARD_DIR, histogram_freq=0, write_graph=True)
print(parallel_model.summary())

hist_current = parallel_model.fit_generator(
                                    data_gen.train_generator(24),
                                    steps_per_epoch=350,
                                    epochs=200,
                                    verbose=1,
                                    validation_data=data_gen.valid_generator(24),
                                    validation_steps= 55,
                                    max_queue_size=100,
                                    workers=2,
                                    use_multiprocessing=True,
                                    # validation_steps=1024,
                                    # validation_steps=324, # 7767
                                    shuffle=False,
                                    callbacks=[tb_callback, checkpoint, reduce_lr],
                                    # use_multiprocessing=True,
                                    initial_epoch=0)

# model.save('/media/yoonseok/Data/ModelBackup/DeepCNN/1_CNN_model.h5')

import Utils.slack
Utils.slack.send_messeage('학습 끝. 빨리 확인하시요.')
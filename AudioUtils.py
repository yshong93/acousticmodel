import os
import collections
import random

import librosa

noise_path = 'Data/_background_noise_'

def __get_dataset__(dir_path):
    numpy_list = collections.deque()
    for (path, dir_, files) in os.walk(dir_path):
        for file_name in files:
            ext = os.path.splitext(file_name)[-1]
            if ext == '.wav':
                file = "%s/%s" % (path, file_name)
                numpy_list.append(file)

    return numpy_list


noise_path_list = __get_dataset__(noise_path)
noise_raw_list = []
for path in noise_path_list:
    raw, sr = librosa.load(path, sr=16000)
    noise_raw_list.append(raw)

def random_noise(raw):
    raw_len = len(raw)
    noise = random.choice(noise_raw_list)

    noise_len = len(noise)

    min_len = random.randrange(0, min(raw_len, noise_len))

    raw_start_ = random.randrange(0, raw_len - min_len)
    noise_start_ = random.randrange(0, noise_len - min_len)

    raw[raw_start_:raw_start_+min_len] += noise[noise_start_: noise_start_+min_len]
    return raw

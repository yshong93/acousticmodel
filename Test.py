# wav_dir_path = '/media/yoonseok/Data/Corpus/Audio/korean_reading/feature5/'
#
# from DataGenerator import DataGenerator
#
# data_gen = DataGenerator(wav_dir_path)
# train_Gen = data_gen.train_generator(14)
# next(train_Gen)

def levenshteinDistance(s1, s2):
    if len(s1) > len(s2):
        s1, s2 = s2, s1

    distances = range(len(s1) + 1)
    for i2, c2 in enumerate(s2):
        distances_ = [i2+1]
        for i1, c1 in enumerate(s1):
            if c1 == c2:
                distances_.append(distances[i1])
            else:
                distances_.append(1 + min((distances[i1], distances[i1 + 1], distances_[-1])))
        distances = distances_
    return distances[-1]

# a = ['M', 'AA', 'L', 'SS', 'IY', 'N', 'WW', 'NN', 'K', 'OW', 'TQ', 'KK', 'WW', 'S', 'AA', 'R', 'AA', 'M', 'WI', 'IY', 'NN', 'PH', 'UW', 'M', 'WW', 'L', 'T', 'WW', 'R', 'AX', 'N', 'EY', 'K', 'EH', 'M', 'AA', 'R', 'JX', 'N', 'AA', 'N', 'IY', 'N', 'IY']
# b = ['TT', 'AA', 'L', 'IY', 'JX', 'WW', 'OW', 'WW', 'L', 'S', 'AA', 'R', 'M', 'IY', 'NN', 'M', 'WW', 'L', 'SS', 'WW', 'R', 'AX', 'N', 'M', 'AA', 'R', 'JX', 'N', 'AA', 'R', 'N', 'IY']

a = 'hello'
b = 'hello'
print(levenshteinDistance(a, b) / len(a))